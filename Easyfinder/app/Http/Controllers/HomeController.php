<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Backend\Bechelor;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.user.home');
    }

    public function room_search(Request $request)
    {
        //dd($request);
        $search=$request->search;
        //dd($search);
        $bechelors = Bechelor::orWhere('title','like','%'.$search.'%')
        ->orWhere('gender','like','%'.$search.'%')
        ->orWhere('status','like','%'.$search.'%')
        ->orWhere('address','like','%'.$search.'%')
        ->orWhere('room_rent','like','%'.$search.'%')
        ->orWhere('month','like','%'.$search.'%')
        ->orWhere('mobile','like','%'.$search.'%')
        ->orWhere('married_status','like','%'.$search.'%')
        ->orWhere('facilities','like','%'.$search.'%')
        ->orWhere('conditions','like','%'.$search.'%')
        ->orWhere('address','like','%'.$search.'%')
        ->orWhere('city_id','like','%'.$search.'%')
        ->orWhere('thana_id','like','%'.$search.'%')
        ->orWhere('ward_id','like','%'.$search.'%')
        ->orWhere('room_rent','like','%'.$search.'%')
        ->orderBy('id','desc')
        ->get();
        $comments = DB::select('SELECT * FROM comments');
        //dd($bechelors);
        return view('backend.admin.Bechelor.index',compact('bechelors','search','comments'));
    }
}
