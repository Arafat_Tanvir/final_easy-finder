<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Card;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use File;

class CardController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.admin.cards.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request,[
            'seat'=>'required',
        ]);

        $cards=new Card();
        if(Auth::check()){
            $cards->user_id=Auth::id();
        }
        $cards->seat=$request->seat;
        $cards->bechelor_id=$request->bechelor_id;
        $cards->ip_address=$request->ip();
        //dd($cards);
        $cards->save();
        return redirect()->route('cards.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cards=Card::findOrFail($id);
        //dd($cards);
        if(!is_null($cards)){
            $cards->seat=$request->seat;
            $cards->update();
            return redirect()->route('cards.index');
        }else{
            return redirect('cards');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cards=Card::findOrFail($id);
        //dd($cards);
        if(!is_null($cards))
        {
            $cards->delete();
            
        }else{
            return redirect('cards');
        }
        session()->flash('success','Product has delete Successfully');
        return back();
    }
}
