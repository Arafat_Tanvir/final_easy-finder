<?php

namespace App\Http\Controllers\Backend;
use DB;
use App\Beckend\District;
use App\Beckend\Division;
use App\Beckend\Upazila;
use App\Beckend\Union;
use App\Backend\City;
use App\Backend\Location;
use App\Backend\Bechelor;
use App\Backend\Comment;
use App\Backend\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    public function divisions_ajax($id)
    {
        $districts = DB::table('districts')->where('division_id', $id)->pluck('name','id');
        return json_encode($districts);
    }

    public function districts_ajax($id)
    {
        //dd($id);
        $upazilas=DB::table("upazilas")
        ->where("district_id",$id)
        ->pluck("name","id");
        return json_encode($upazilas);
    }

    public function upazilas_ajax($id)
    {
        $unions=DB::table("unions")
        ->where("upazila_id",$id)
        ->pluck("name","id");
        return json_encode($unions);
    }

    public function search()
    {
        //dd("sfdkasf");
        $cities=City::orderBy('id','desc')->get();
        $categories=Category::orderBy('id','desc')->get();
        //dd($cities);
        return view('backend.admin.bechelor.search',compact('cities','categories'));
    }

    public function search_post(Request $request)
    {
        //dd('afgdaskf');
        $search = new Location();
        //dd($search);
        $city_id=$search->city_id = $request->city_id;
        $thana_id=$search->thana_id = $request->thana_id;
        $ward_id=$search->ward_id = $request->ward_id;
        $category_id=$search->category_id = $request->category_id;
        //dd($category_id);
        $bechelors = Bechelor::where('city_id', $city_id)
        ->where('thana_id',$thana_id)
        ->where('ward_id',$ward_id)
        ->where('category_id',$category_id)
        ->get();
        $categories = Category::all();
        $comments=Comment::all();
        $result=count($bechelors);
        //dd($result);

        if($result>0){
            return view('backend.admin.bechelor.index', compact('bechelors','comments','categories'));
        }else{
            return redirect('search')->with('message','No Seat or Room Availbale');
        }

        
    }
}
