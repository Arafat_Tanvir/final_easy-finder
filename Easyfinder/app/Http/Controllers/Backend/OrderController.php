<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Order;
use App\Backend\Card;
use App\Backend\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class OrderController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $orders = Order::orderBy('id','desc')->get();
      //dd($orders);
      return view('backend.admin.orders.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'payment_method_id' => 'required',

        ]);

        $order=new Order();
        //dd($order);
        $order->name=$request->name;
        $order->email=$request->email;
        $order->phone=$request->phone;;
        $order->street_address=$request->street_address;
        $order->message=$request->message;
        $order->ip_address=$request->ip();
        $order->transaction_id=$request->transaction_id;
        //dd($order);


        if (Auth::check()) {
            $order->user_id=Auth::id();
        }

        if($request->payment_method_id!='Cash'){
            if($request->transaction_id==NULL || empty($request->transaction_id)){
                session()->flash('sticky_error','Please input transaction id for your payment');
                return back();
            }
        }
        $order->payment_id=Payment::where('short_name',$request->payment_method_id)->first()->id;
        //dd($order);
        $order->save();

        foreach (Card::total_Cards() as $cards) {
            $cards->order_id=$order->id;
            $cards->save();
        }
        return redirect()->route('bechelors.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //dd($id);
      $orders=Order::findOrFail($id);
      $orders->is_seen_by_admin=1;
      $orders->save();
      return view('backend.admin.orders.show',compact('orders'));

    }

    public function orderPaid($id)
    {
      $orders=Order::findOrFail($id);
      if($orders->is_paid){
        $orders->is_paid=0;
      }else{
        $orders->is_paid=1;
      }
      $orders->save();
      session()->flash('success','Order status is change!');
      return back();

    }

    public function orderComplete($id)
    {
      $orders=Order::findOrFail($id);
      if($orders->is_complete){
        $orders->is_complete=0;
      }else{
        $orders->is_complete=1;
      }

      $orders->save();
      session()->flash('success','Order status is change!');
      return back();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $orders = Order::findOrFail($id);
      return view('Orders.edit',compact('orders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,
          [
          'title'=>'required|max:50|min:5',
          'description'=>'required',
          'price' =>'required',

          ]
  );

      $orders=Order::findOrFail($id);
      //dd($orders);
      $orders->title=$request->title;
      $orders->description=$request->description;
      $orders->slug=str_slug($request->title);
      $orders->quantity=$request->quantity;
      $orders->price=$request->price;
      $orders->status=$request->status;
      $orders->offer_price=$request->offer_price;
      $orders->brand_id=$request->brand_id;
      $orders->category_id=$request->category_id;
      $orders->user_id=Auth::user()->id;

      $orders->update();
      if($orders){
          Session::flash('status','Order Created Successfully');
      return redirect('products');
      }else{
          Session::flash('status','Order not Created');
      return redirect()->back();
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $orders=Order::findOrFail($id);
      if(!is_null($orders))
      {
          $orders->delete();
      }
      session()->flash('success','Order has delete Successfully');
      return back();
    }
}
