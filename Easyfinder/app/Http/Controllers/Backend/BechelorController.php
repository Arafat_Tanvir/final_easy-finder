<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Backend\City;
use App\Backend\Ward;
use App\Backend\Thana;
use App\Backend\Category;
use App\Backend\BechelorImage;
use Illuminate\Support\Facades\Auth;
use App\Backend\Bechelor;
use App\Backend\Comment;
use Image;
use DB;

class BechelorController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bechelors=Bechelor::orderBy('id','desc')->get();
        $comments = DB::select('SELECT * FROM comments');
        //dd($comments);
        return view('backend.admin.bechelor.index',compact('bechelors','comments'));
    }

    public function room_details($id){
        $bechelor = Bechelor::where('id', $id)
        ->orderBy('id','desc')
        ->get();
        $comments=Comment::all();
        //dd($bechelor);
        return view('Bechelor.index',compact('bechelor','comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities=City::orderBy('id','desc')->get();
        $categories=Category::orderBy('id','desc')->get();
        return view('backend.admin.bechelor.create',compact('cities','categories'));
    }

    public function cities_ajax($id)
    {
        $thanas = DB::table('thanas')->where('city_id', $id)->pluck('name','id');
        return json_encode($thanas);
    }

    public function thanas_ajax($id)
    {
        //dd($id);
        $wards=DB::table("wards")
        ->where("thana_id",$id)
        ->pluck("name","id");
        return json_encode($wards);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bechelor=new Bechelor;
        
        $bechelor->title=$request->title;
        $bechelor->month=$request->month;
        $bechelor->status=$request->status;
        $bechelor->gender=$request->gender;
        $bechelor->religion=$request->religion;
        $bechelor->married_status=$request->married_status;
        $bechelor->seat=$request->seat;
        $bechelor->room_type=$request->room_type;
        $bechelor->category_id=$request->category_id;
        $bechelor->room_rent=$request->room_rent;
        $bechelor->facilities= implode(',', $request['facilities']);
        $bechelor->conditions= implode(',', $request['conditions']);
        $bechelor->mobile=$request->mobile;
        $bechelor->city_id=$request->city_id;
        $bechelor->thana_id=$request->thana_id;
        $bechelor->ward_id=$request->ward_id;
        $bechelor->address=$request->address;

        if (Auth::check()) {
            $bechelor->user_id=Auth::id();
        }
        $bechelor->save();

        if(count($request->images)>0){
            foreach ($request->images as $images) {
                $img=time().'.'.$images->getClientOriginalExtension();
                $location=public_path('images/bechelor_room/'.$img);
                Image::make($images)->save($location);

                $bechelor_images=new BechelorImage();
                $bechelor_images->images=$img;
                $bechelor_images->bechelor_id=$bechelor->id;
                $bechelor_images->save();
            }
        }
        

    }

    public function upload($file){
        //$extension = $file->getClientOrginalExtension();
        $name =time().$file->getClientOriginalName();
        $filename=$name;
        $current = public_path('image_bechelor/');
        $file->move($current, $filename);
        return 'image_bechelor/'.$filename;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bechelor = Bechelor::findOrFail($id);
        $comments=Comment::all();
        return view('bechelor.show',compact('bechelor','comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bechelors = Bechelor::findOrFail($id);
        $facilities = explode(",", $bechelors->facilities);
        $conditions = explode(",", $bechelors->conditions);
        $cities=City::all();
        return view('bechelor.edit',compact('bechelors','cities','facilities','conditions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bechelors = Bechelor::findOrFail($id);
        $input =$request->all();
        if(isset($input['image'])){
            $input['image'] = $this->upload($input['image']);
        }
        $input['facilities'] = implode(',', $input['facilities']);
        $input['conditions'] = implode(',', $input['conditions']);
        $result=$bechelors->update($input);
        if($result){
            return redirect('bechelor')->with('message','Update Successfully');
        }else{
            return beck()->with('message','Some Error Occar');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
