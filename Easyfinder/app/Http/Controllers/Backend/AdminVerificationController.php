<?php

namespace App\Http\Controllers\Backend;
use App\Backend\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminVerificationController extends Controller
{
    public function verify($token)
    {

    	$admin=Admin::where('remember_token',$token)->first();
    	if (!is_null($admin)) {
    		$admin->status=1;
    	    $admin->remember_token=NULL;
    	    $admin->save();
    	    session()->flash('success','you are registered successfully::login now');
    	    return redirect()->route('admin.login');
    	}else{
    		session()->flash('errors','Sorry! Your token is not match!!');
    		return redirect('/admin');
    	}
    	
    }
}
