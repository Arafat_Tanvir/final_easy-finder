<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\Payment;

class CheckoutController extends Controller
{
    
    public function index()
    {
        $payments = Payment::orderBy('id','asc')->get();
        //dd($payments);
        return view('backend.admin.Checkout.index',compact('payments'));
    }
}
