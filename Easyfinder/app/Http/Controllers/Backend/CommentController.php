<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Backend\Comment;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment=new Comment;
        $comment->body=$request->body;
        $comment->bechelor_id=$request->bechelor_id;
        if (Auth::check()) {
            $comment->user_id=Auth::id();
        }
        $comment->save();
        
    }
}
