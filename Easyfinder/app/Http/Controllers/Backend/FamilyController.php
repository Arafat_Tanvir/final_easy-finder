<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backend\FamilyImage;
use App\Backend\Family;
use App\Backend\Category;
use App\Backend\City;
use App\Backend\Ward;
use App\Backend\Thana;
use App\Backend\Admin;
use Illuminate\Support\Facades\Auth;
use File;
use Image;
use DB;

class FamilyController extends Controller
{
    public function index()
    {
    	//dd("dsfkas");
    	$families=Family::orderBy('id','desc')->get();
        return view('backend.admin.family.index',compact('families'));
    }

    public function create(){
    	$cities=City::orderBy('id','desc')->get();
        $categories=Category::orderBy('id','desc')->get();
    	return view('backend.admin.family.create',compact('cities','categories'));
    }


    public function cities_ajax($id)
    {
        $thanas = DB::table('thanas')->where('city_id', $id)->pluck('name','id');
        return json_encode($thanas);
    }

    public function thanas_ajax($id)
    {
        //dd($id);
        $wards=DB::table("wards")
        ->where("thana_id",$id)
        ->pluck("name","id");
        return json_encode($wards);
    }

    public function store(Request $request){
        //dd('afdsaf');
        $this->validate($request,[
            'title'=>'required|max:50|min:5',
            'month'=>'required',
            'religion'=>'required',
            'room_status'=>'required',
            'total_room'=>'required',
            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'room_size'=>'required',
            'drawing_room'=>'required',
            'room_rent'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'mobile'=>'required',
            'description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required'

        ]);

        $family=new Family;
        $family->title=$request->title;
        $family->month=$request->month;
        $family->room_status=$request->room_status;
        $family->total_room=$request->total_room;
        $family->religion=$request->religion;
        $family->bed_room=$request->bed_room;
        $family->bath_room=$request->bath_room;
        $family->dinning_room=$request->dinning_room;
        $family->category_id=$request->category_id;
        $family->room_size=$request->room_size;
        $family->drawing_room=$request->drawing_room;
        $family->description=$request->description;
        $family->room_rent=$request->room_rent;
        $family->facilities= implode(',', $request['facilities']);
        $family->conditions= implode(',', $request['conditions']);
        $family->mobile=$request->mobile;
        $family->city_id=$request->city_id;
        $family->thana_id=$request->thana_id;
        $family->ward_id=$request->ward_id;
        $family->address=$request->address;
        $family->save();

        if(count($request->images)>0){
            foreach ($request->images as $images) {
                $img=time().'.'.$images->getClientOriginalExtension();
                $location=public_path('images/family_room/'.$img);
                Image::make($images)->save($location);

                $family_images=new familyImage();
                $family_images->images=$img;
                $family_images->family_id=$family->id;
                $family_images->save();
            }
        }
        

    }
    public function edit($id)
    {

        $families = Family::findOrFail($id);
        $facilities = explode(",", $families->facilities);
        $conditions = explode(",", $families->conditions);
        $cities=City::all();
        $categories=Category::all();
        
        return view('backend.admin.family.edit',compact('families','cities','facilities','conditions','categories'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required|max:50|min:5',
            'month'=>'required',
            'religion'=>'required',
            'room_status'=>'required',
            'total_room'=>'required',
            'bed_room'=>'required',
            'bath_room'=>'required',
            'dinning_room'=>'required',
            'room_size'=>'required',
            'drawing_room'=>'required',
            'room_rent'=>'required',
            'facilities'=>'required',
            'conditions'=>'required',
            'mobile'=>'required',
            'description'=>'required',
            'category_id'=>'required',
            'city_id'=>'required',
            'thana_id'=>'required',
            'ward_id'=>'required',
            'address'=>'required'

        ]);

        $family = Family::find($id);
        $family->title=$request->title;
        $family->month=$request->month;
        $family->room_status=$request->room_status;
        $family->total_room=$request->total_room;
        $family->religion=$request->religion;
        $family->bed_room=$request->bed_room;
        $family->bath_room=$request->bath_room;
        $family->dinning_room=$request->dinning_room;
        $family->category_id=$request->category_id;
        $family->room_size=$request->room_size;
        $family->drawing_room=$request->drawing_room;
        $family->description=$request->description;
        $family->room_rent=$request->room_rent;
        $family->facilities= implode(',', $request['facilities']);
        $family->conditions= implode(',', $request['conditions']);
        $family->mobile=$request->mobile;
        $family->city_id=$request->city_id;
        $family->thana_id=$request->thana_id;
        $family->ward_id=$request->ward_id;
        $family->address=$request->address;
        $family->update();
    }
}
