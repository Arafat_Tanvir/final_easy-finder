<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['body','parent_id','bechelor_id','user_id'];

	public function division(){
	    return $this->belongsTo(Division::class);
	  }
}
