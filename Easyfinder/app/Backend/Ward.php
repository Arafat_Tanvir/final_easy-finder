<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $fillable = ['name','bangla_name','latitude','longitude','thana_id'];

    public function thana(){
    	return $this->belongsTo(Thana::class);
    }
}
