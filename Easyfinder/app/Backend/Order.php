<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name','email','phone','message','ip_address','street_address','is_paid','is_seen_by_admin','is_complete','transaction_id','user_id','payment_id'];

    public function users(){
    	return $this->belongsTo(User::class);
    }

    public function cards(){
    	return $this->HasMany(Card::class);
    }

    public function payment(){
    	return $this->belongsTo(Payment::class);
    }
}
