<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
  protected $fillable = ['name','bangla_name','latitude','longitude','division_id'];

  public function division(){
    return $this->belongsTo(Division::class);
  }
}
