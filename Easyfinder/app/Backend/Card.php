<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Backend\Order;
use App\Backend\Card;

class Card extends Model
{
    protected $fillable = ['seat','ip_address','bechelor_id','user_id','order_id'];

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function order(){
    	return $this->belongsTo(Order::class);
    }

    public function bechelor(){
    	return $this->belongsTo(Bechelor::class);
    }

//total card are vallues in this section 
    public static function total_Items()
    {
        if(Auth::check()){
            $cards=Card::where('user_id',Auth::id())
            ->where('order_id',NULL)
            ->get();
            //dd($cards);
        }else{

             $cards=Card::where('ip_address',request()->ip())
             ->where('order_id',NULL)
             ->get();
            //dd($cards);
        }
        $total_items=0;
        foreach ($cards as $card) {
            $total_items+=$card->seat;
        }
        return $total_items;
    }

    //total card are show in this section 
    public static function total_Cards()
    {
        if(Auth::check()){

            $cards=Card::where('user_id',Auth::id())
            ->where('order_id',NULL)
            ->get();
            //dd($cards);
        }else{
             $cards=Card::where('ip_address', request()->ip())
             ->where('order_id',NULL)
             ->get();
             //dd($cards);
        }
        return $cards;
    }
}
