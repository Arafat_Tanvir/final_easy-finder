<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Thana extends Model
{
    protected $fillable = ['name','bangla_name','latitude','longitude','city_id'];

    public function city(){
    	return $this->belongsTo(City::class);
    }
}
