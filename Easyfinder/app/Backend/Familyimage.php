<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Familyimage extends Model
{
    protected $fillable = ['images','family_id'];
}
