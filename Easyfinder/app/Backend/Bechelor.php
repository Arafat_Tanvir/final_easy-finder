<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Bechelor extends Model
{
	protected $fillable = ['title','month', 'status', 'gender', 'religion', 'married_status','seat', 'room_type', 'room_rent','facilities', 'conditions','mobile','address','category_id', 'city_id', 'thana_id', 'ward_id', 'user_id'];

	public function images()
	   {
	   	return $this->hasMany('App\Backend\Bechelorimage');
	   }

	 public function user()
	{
    	return $this->belongsTo('App\User');
    }


	  public function city(){
	    return $this->belongsTo(City::class);
	  }
	  public function thana(){
	    return $this->belongsTo(Thana::class);
	  }
	  public function ward(){
	    return $this->belongsTo(Ward::class);
	  }
    
}
 