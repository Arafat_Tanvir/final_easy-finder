<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Upazila extends Model
{
  protected $fillable = ['name','bangla_name','latitude','longitude','district_id'];

  public function district(){
    return $this->belongsTo(District::class);
  }
}
