<?php

namespace App\Backend;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $fillable = ['title','month','religion','room_status', 'total_room',  'bed_room', 'bath_room','dinning_room', 'room_rent','drawing_room','room_size','facilities', 'conditions','category_id', 'city_id', 'thana_id', 'ward_id', 'admin_id','description','mobile','address',];

	public function images()
	   {
	   	return $this->hasMany('App\Backend\Familyimage');
	   }

	 public function user()
	{
    	return $this->belongsTo('App\Backend\Admin');
    }


	  public function city(){
	    return $this->belongsTo(City::class);
	  }
	  public function thana(){
	    return $this->belongsTo(Thana::class);
	  }
	  public function ward(){
	    return $this->belongsTo(Ward::class);
	  }
}
