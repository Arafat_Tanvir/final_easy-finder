@extends('backend.user.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin {{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.register') }}"  enctype="multipart/form-data">
                      {{ csrf_field() }}

                       <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('address') }}</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required autofocus>

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="images" class="col-md-4 col-form-label text-md-right">{{ __('images') }}</label>

                            <div class="col-md-6">
                                <input id="images" type="file" class="form-control{{ $errors->has('images') ? ' is-invalid' : '' }}" name="images" value="{{ old('images') }}" required autofocus>

                                @if ($errors->has('images'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    $('select[name="division_id"]').on('change',function(){
      var division_id=$(this).val();
      //alert(division_id);
      if(division_id){
        $.ajax({
          url:'{{ url('')}}/divisions/ajax/'+division_id,
          type:"GET",
          dataType:"json",
          success:function(data){
            $('select[name="district_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="district_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="district_id"]').empty();
        }
    });
    $('select[name="district_id"]').on('change',function(){
      var district_id=$(this).val();
      //alert(district_id);
      console.log(district_id);
      if(district_id){
        $.ajax({
          url:'{{ url('')}}/districts/ajax/'+district_id,
          type:"GET",
          dataType:"json",
          success:function(data){
          $('select[name="upazila_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="upazila_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="upazila_id"]').empty();
      }
  });
    $('select[name="upazila_id"]').on('change',function(){
      var upazila_id=$(this).val();
      //alert(upazila_id);
      console.log(upazila_id);
      if(upazila_id){
        $.ajax({
          url:'{{ url('')}}/upazilas/ajax/'+upazila_id,
          type:"GET",
          dataType:"json",
          success:function(data){
          $('select[name="union_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="union_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="union_id"]').empty();
      }
  });
});
</script>
@endsection