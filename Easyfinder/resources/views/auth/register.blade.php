@extends('backend.user.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}"  enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('first_name') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('last_name') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group{{ $errors->has('division_id') ? ' has-error' : '' }}">
                              <label for="division_id" class="col-md-12 control-label">Division</label>
                              <div class="col-md-12">
                                  <select name="division_id" id="division_id" class="form-control">
                                      <option value="0" disabled="true" selected="true">===Choose Divisio Name==</option>
                                      @foreach(App\Backend\Division::orderBy('name','desc')->get(); as $division)
                                      <option value="{{$division->id}}">{{$division->name}}</option>
                                      @endforeach
                                    </select>
                                  @if ($errors->has('division_id'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('division_id') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('district_id') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="district_id" class="control-label">District</label>
                                <div class="input-group">
                                    <select name="district_id" id="district_id" class="form-control">
                                      <option value="0" disabled="true" selected="true">===Choose District name==</option>
                                    </select>
                                    @if ($errors->has('district_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('district_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                          </div>
                          <div class="col-sm-6">
                             <div class="form-group{{ $errors->has('upazila_id') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="upazila_id" class="control-label">Upazila</label>
                                <div class="input-group">
                                    <select name="upazila_id" id="upazila_id" class="form-control">
                                      <option value="0" disabled="true" selected="true">===Choose Upazila Name==</option>
                                    </select>
                                    @if ($errors->has('upazila_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('upazila_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('union_id') ? ' has-error' : '' }}">
                            
                            <div class="col-md-12">
                                <label for="union_id" class="control-label">Union</label>
                                <div class="input-group">
                                    <select name="union_id" id="union_id" class="form-control">
                                      <option value="0" disabled="true" selected="true">===Choose Union Name==</option>
                                    </select>
                                    @if ($errors->has('union_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('union_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                          </div>
                        </div>


                        <div class="form-group row">
                            <label for="street_address" class="col-md-4 col-form-label text-md-right">{{ __('street_address') }}</label>

                            <div class="col-md-6">
                                <input id="street_address" type="text" class="form-control{{ $errors->has('street_address') ? ' is-invalid' : '' }}" name="street_address" value="{{ old('street_address') }}" required autofocus>

                                @if ($errors->has('street_address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('street_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="images" class="col-md-4 col-form-label text-md-right">{{ __('images') }}</label>

                            <div class="col-md-6">
                                <input id="images" type="file" class="form-control{{ $errors->has('images') ? ' is-invalid' : '' }}" name="images" value="{{ old('images') }}" required autofocus>

                                @if ($errors->has('images'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    $('select[name="division_id"]').on('change',function(){
      var division_id=$(this).val();
      //alert(division_id);
      if(division_id){
        $.ajax({
          url:'{{ url('')}}/divisions/ajax/'+division_id,
          type:"GET",
          dataType:"json",
          success:function(data){
            $('select[name="district_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="district_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="district_id"]').empty();
        }
    });
    $('select[name="district_id"]').on('change',function(){
      var district_id=$(this).val();
      //alert(district_id);
      console.log(district_id);
      if(district_id){
        $.ajax({
          url:'{{ url('')}}/districts/ajax/'+district_id,
          type:"GET",
          dataType:"json",
          success:function(data){
          $('select[name="upazila_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="upazila_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="upazila_id"]').empty();
      }
  });
    $('select[name="upazila_id"]').on('change',function(){
      var upazila_id=$(this).val();
      //alert(upazila_id);
      console.log(upazila_id);
      if(upazila_id){
        $.ajax({
          url:'{{ url('')}}/upazilas/ajax/'+upazila_id,
          type:"GET",
          dataType:"json",
          success:function(data){
          $('select[name="union_id"]').empty();
            $.each(data,function(key,value){
            $('select[name="union_id"]').append('<option value="'+key+'">'+value+'</option>')
            });
          }
        });
      }else{
        $('select[name="union_id"]').empty();
      }
  });
});
</script>
@endsection
