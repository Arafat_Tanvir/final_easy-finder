@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="card mx-auto mt-5">
    <div class="card-header">
      <h2>City Create Form <span class="pull-right"><a href="{{ route('districts.index')}}">Back</a></span></h2>
    </div>
    <div class="card-body">
      <h4>Divisions Details<a href="{{route('divisions.index')}}" class="btn btn-primary pull-right">Back</a></h4>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-4">
                   <p>name</p>
                   <p>Bangla Name</p>
                   <p>Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-8">
                   <p>{{$divisions->name}} </p>
                   <p>{{$divisions->bangla_name?$divisions->bangla_name:'N\A'}} </p>
                   <p>{{$divisions->latitude?$divisions->latitude:'N\A'}} </p>
                   <p>{{$divisions->longitude?$divisions->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div id="map">

  </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
