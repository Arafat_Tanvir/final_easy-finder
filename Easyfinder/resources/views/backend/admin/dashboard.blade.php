@extends('Backend.admin.layouts.master')


@section('content')
<div class="advanced-form-area mg-tb-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                 <h2 class="pull-right">{{ Auth::user()->name }}</h2>
            </div>
        </div>
    </div>
</div>
@endsection