@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="card mx-auto mt-5">
    <div class="card-header">
      <h2>Unions Create Form <span class="pull-right"><a href="{{ route('unions.index')}}">Back</a></span></h2>
    </div>
    <div class="card-body">
      <h4>Uinon Details<a href="{{route('unions.index')}}" class="btn btn-primary pull-right">Back</a></h4>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-4">
                   <p>Thana name</p>
                   <p>Thana Bangla Name</p>
                   <p>Thana Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-8">
                   <p>Division: {{$unions->upazila->name}}, Uinon: {{$unions->name}}</p>
                   <p>{{$unions->bangla_name?$unions->bangla_name:'N\A'}} </p>
                   <p>{{$unions->latitude?$unions->latitude:'N\A'}} </p>
                   <p>{{$unions->longitude?$unions->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
        </div>
        <div id="map">

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
