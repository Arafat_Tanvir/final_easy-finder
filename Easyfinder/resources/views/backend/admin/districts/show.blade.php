@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="card mx-auto mt-5">
    <div class="card-header">
      <h2>City Create Form <span class="pull-right"><a href="{{ route('districts.index')}}">Back</a></span></h2>
    </div>
    <div class="card-body">
      <h4>District Details<a href="{{route('districts.index')}}" class="btn btn-primary pull-right">Back</a></h4>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-4">
                   <p>Thana name</p>
                   <p>Thana Bangla Name</p>
                   <p>Thana Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-8">
                   <p>Division: {{$districts->division->name}}, District: {{$districts->name}}</p>
                   <p>{{$districts->bangla_name?$districts->bangla_name:'N\A'}} </p>
                   <p>{{$districts->latitude?$districts->latitude:'N\A'}} </p>
                   <p>{{$districts->longitude?$districts->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div id="map">

  </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
