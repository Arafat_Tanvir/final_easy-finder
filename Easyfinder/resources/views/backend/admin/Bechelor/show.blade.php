@extends('layouts.app')

@section('content')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="box-title" style="text-align: center;font-size: 30px;color: white;background-color: #0321fd"><strong>{{$bechelor_room->seat}} Roommate Wanted,From {{$bechelor_room->date}} at {{$bechelor_room->wards->ward}}</strong></h4>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <h4><span>Type</span></h4>
                                                        <h4><span>Gender</span></h4>
                                                        <h4><span>Religion</span></h4>
                                                        <h4><span>Status</span></h4>
                                                        <h4><span>City</span></h4>
                                                        <h4><span>Thana</span></h4>
                                                        <h4><span>Contact</span></h4>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <h4><span>{{$bechelor_room->status}}</span></h4>
                                                        <h4><span>{{$bechelor_room->gender}}</span></h4>
                                                        <h4><span>{{$bechelor_room->religion}}</span></h4>
                                                        <h4><span>{{$bechelor_room->married}}</span></h4>
                                                        <h4><span>{{$bechelor_room->cities->city}}</span></h4>
                                                        <h4><span>{{$bechelor_room->thanas->thana}}</span></h4>
                                                        <h4><span>{{$bechelor_room->mobile}}</span></h4>
                                                    </div>
                                                </div>
                                                <h4 style="padding-top: 20px"><strong>Addeess:</strong> <span>{{$bechelor_room->address}}</span></h4>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-5">
                                                        <h4><span></span></h4>
                                                        <h4><span>Room</span></h4>
                                                        <h4><span>Room Type</span></h4>
                                                        <h4><span>Room Rent</span></h4>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <h4><span class="pull-right"><a href="{{route('bechelor.booking',$bechelor_room->id)}}" class="btn btn-success btn-sm">Now book</a></span></h4>
                                                        <h4><span>{{$bechelor_room->room}}</span></h4>
                                                        <h4><span>{{$bechelor_room->room_type}}</span></h4>
                                                        <h4><span>{{$bechelor_room->room_rent}}</span></h4>
                                                    </div>
                                                </div>
                                                <h4 style="padding-top: 20px"><strong>Facilities:</strong> <span>{{$bechelor_room->facilities}}</span></h4>
                                                <h4 style="padding-top: 20px"><strong>Conditions:</strong> <span>{{$bechelor_room->conditions}}</span></h4>
                                            </div>
                                        </div>
                                        <p style="padding-top: 20px"><span>Created By : <a href="{{route('user_details.show', $bechelor_room->user->id)}}">{{$bechelor_room->user->name}}</a></span> <span class="pull-right">Created at: {{$bechelor_room->created_at->toFormattedDateString()}}</span></p>
                                        <div class="card-header">
                                                <h4><strong>Comments</strong></h4>
                                        </div>
                                            @foreach($comments as $comment)
                                                    @if($bechelor_room->id==$comment->bechelor_comment_id)
                                                    <p style="color: green"><span><a href="{{route('user_details.show', $comment->user->id)}}"><span style="color: green;"><b>{{$comment->user->name}}</b></span></a></span><span class="pull-right">{{$comment->created_at->toFormattedDateString()}}</span></p>
                                                    <p>
                                                        <span>{{ $comment->body }}</span>
                                                        <span class="pull-right">
                                                            @if(Auth::user()->email==$comment->user->email)
                                                            <span><a href="{{route('comment.edit', $comment->id)}}"><i class="fa fa-pencil"></i></a></span>
                                                            @endif
                                                        </span> 
                                                    </p>
                                                    @endif
                                            @endforeach
                                        <!-- <h4>Add comment</h4> -->
                                        <form method="post" action="{{ route('comment.store') }}">
                                            {{ csrf_field()}}
                                            <input type="hidden" name="bechelor_comment_id" value="{{ $bechelor_room->id }}" />
                                            <label for="body">Add Comment</label>
                                                <textarea name="body" id="body" class="form-control" cols="30" rows="5"></textarea>
                                                <p class="help-block">{{ ($errors->has('body')) ? $errors->first('body') : ''}}</p>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-warning" value="submit">Comment</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card-body">
                                        @if($bechelor_room->image!="")
                                        <div class="portfolio-item">
                                            <div class="portfolio-item-inner">
                                                <img class="img-responsive" src="{{asset($bechelor_room->image)}}" alt="">
                                                    <div class="portfolio-info">
                                                        <a class="preview" href="{{asset($bechelor_room->image)}}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @if(Auth::user()->email==$bechelor_room->user->email)
                                        <p style="margin-top: 20px">
                                             <span class="btn btn-danger btn-sm"><a href="{{route('show_request.show_request', $bechelor_room->id)}}" style="color: #fff"> Show Request</a></span>
                                            <span class="btn btn-warning pull-right"><a href="{{route('bechelor_room.edit', $bechelor_room->id)}}" ><i class="fa fa-edit fa-lg" style="color: #fff"></i></a></span>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <h4 style="text-align: center; color: green">===========Bechelor Roommate Wanted============</h4>
                            <div class="card-body"></div>
                        </div>
                    </div>
                </div>
@endsection