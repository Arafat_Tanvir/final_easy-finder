@extends('backend.admin.layouts.master')
@section('content')
<div class="container">
    <div class="card mx-auto mt-5">
        @foreach($bechelors as $bechelor)
        <div class="card">
            <div class="card-header">
                <p class="small text-center text-muted mt-2">
                    <img src="{{asset('images/bechelor_room/bechelor_room.jpg')}}" height="80px" width="100%">
                </p>
            </div>
            <div class="card-body">

                <h4><span class="badge badge-pill badge-success mt-2">ID=BRW{{$bechelor->id}}</span> <span class="small text-muted">@include('backend/admin/Cards/card-button')</span></h4>
                <div class="row">
                    <div class="col-lg-8" style=height: 100%">
                        <div class="row">
                            <div class="col-lg-8">
                                <strong class="badge badge-light">Room Details</strong>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <span>Title</span><br>
                                        <span>Month</span><br>
                                        <span>Status</span><br>
                                        <span>Seat</span><br>
                                        <span>Room Type</span><br>
                                        <span>Rent</span><br>
                                        <span>Gender</span><br>
                                        <span>Religion</span><br>
                                        <span>City</span><br>
                                        <span>Thana</span><br>
                                        <span>Ward</span><br>
                                        <span>Contact</span><br>
                                    </div>
                                    <div class="col-lg-8">
                                        <span>{{$bechelor->title}}</span><br>
                                        <span>{{$bechelor->month}},2019</span><br>
                                        <span>{{$bechelor->status}}</span><br>
                                        <span>{{$bechelor->seat}}</span><br>
                                        <span>{{$bechelor->room_type}}</span><br>
                                        <span>{{$bechelor->room_rent}}(per seat)</span><br>
                                        <span>{{$bechelor->gender}}</span><br>
                                        <span>{{$bechelor->married_status}}</span><br>
                                        <span>{{$bechelor->city->name}}</span><br>
                                        <span>{{$bechelor->thana->name}}</span><br>
                                        <span>{{$bechelor->ward->name}}</span><br>
                                        <span>{{$bechelor->mobile}}</span><br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4" style="height: 100%" >
                                <strong class="badge badge-light">Facilities</strong>
                                <ul>
                                    <li>{{$bechelor->facilities}}</li>
                                </ul>
                                <strong class="badge badge-warning">Conditions</strong>
                                <ul>
                                    <li>{{$bechelor->conditions}}</li>
                                </ul>
                                <strong class="badge badge-info">Address</strong>
                                <ul>
                                    <li>{{$bechelor->address}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4" style="height: 100%">
                        <strong class="badge badge-light">Room Images</strong>
                        <div id="demo" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-inner card">
                                    @php
                                    $i=1
                                    @endphp
                                    @foreach($bechelor->images as $image)
                                    <div style="text-align: center;" class="carousel-item {{$i==1 ? 'active' : ''}}">
                                        <img class="card-img-top-image" src="{{asset('images/bechelor_room/'.$image->images)}}" alt="" height=200">
                                    </div>
                                    @php
                                    $i++
                                    @endphp
                                    @endforeach
                                </div>
                            </div>
                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                        <strong class="badge badge-dark">Comments</strong>
                        @foreach($comments as $comment)
                                @if($bechelor->id==$comment->bechelor_id)
                                    <ol>{{ $comment->body }}</ol>
                                @endif
                        @endforeach
                        <form method="post" action="{{ route('comments.store') }}">
                            {{ csrf_field()}}
                            <input type="hidden" name="bechelor_id" value="{{ $bechelor->id }}" />
                            <textarea name="body" id="body" class="form-control" cols="30" rows="1"></textarea>
                            <p class="help-block">{{ ($errors->has('body')) ? $errors->first('body') : ''}}</p>
                            <div class="form-group">
                                <button type="submit" class="badge badge-success" value="submit"><i class="fas fa-comments"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection