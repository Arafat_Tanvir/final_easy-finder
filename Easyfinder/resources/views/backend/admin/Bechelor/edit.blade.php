@extends('layouts.app')
@section('content')
<script type="text/javascript" src="{{ url('')}}/assets/js/jquery-3.2.1.min.js"></script>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title mb-3">Update Post</strong>
          </div>
          <div class="row">
            <div class="col-sm-4">
              @if($bechelor_rooms->image!="")
              <img src="{{asset($bechelor_rooms->image)}}" alt="Card image cap" width="380px" height="500px">
            @endif
            </div>
            <div class="col-sm-8">
              <form role="form" action="{{ route('bechelor_room.update',$bechelor_rooms->id)}}" method="post" enctype="multipart/form-data">
                {{ csrf_field()}}
                {{method_field('PUT')}}
                <div class="form-group">
                  <label for="date">Select Status</label>
                  <select name="date" id="date" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===whem Seat OR Room Available===</option>
                    <option value="1ST JANUARY"@if ($bechelor_rooms->date == "1ST JANUARY")selected="selected" @endif >1ST JANUARY</option>
                    <option value="1ST FEBRUARY" @if ($bechelor_rooms->date == "1ST FEBRUARY")selected="selected" @endif >1ST FEBRUARY </option>
                    <option value="1ST MARCH" @if ($bechelor_rooms->date == "1ST MARCH")selected="selected" @endif > 1ST MARCH </option>
                    <option value="1ST APRIL" @if ($bechelor_rooms->date == "1ST APRIL")selected="selected" @endif > 1ST APRIL </option>
                    <option value="1ST MAY" @if ($bechelor_rooms->date == "1ST MAY")selected="selected" @endif > 1ST MAY </option>
                    <option value="1ST JUNE" @if ($bechelor_rooms->date == "1ST JUNE")selected="selected" @endif >1ST JUNE </option>
                    <option value="1ST JULY" @if ($bechelor_rooms->date == "1ST JULY")selected="selected" @endif >1ST JULY </option>
                    <option value="1ST AUGUST" @if ($bechelor_rooms->date == "1ST AUGUST")selected="selected" @endif >1ST AUGUST </option>
                    <option value="1ST SEPTEMBER" @if ($bechelor_rooms->date == "1ST SEPTEMBER")selected="selected" @endif >1ST SEPTEMBER </option>
                    <option value="1ST OCTOBER" @if ($bechelor_rooms->date == "1ST OCTOBER")selected="selected" @endif >1ST OCTOBER </option>
                    <option value="1ST NOVEMBER" @if ($bechelor_rooms->date == "1ST NOVEMBER")selected="selected" @endif >1ST NOVEMBER </option>
                    <option value="1ST DECEMBER" @if ($bechelor_rooms->date == "1ST DECEMBER")selected="selected" @endif >1ST DECEMBER </option>
                    
                  </select>
                  <p class="help-block">{{ ($errors->has('status')) ? $errors->first('status') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Select Status</label>
                  <select name="status" id="status" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===Choose Status===</option>
                    <option value="Student" @if ($bechelor_rooms->status == "Student")selected="selected" @endif >Student</option>
                    <option value="Job_holder"@if ($bechelor_rooms->status == "Job_holder")selected="selected" @endif >Job Holder</option>
                    <option value="Teacher"@if ($bechelor_rooms->status == "Teacher")selected="selected" @endif >Teacher</option>
                    <option value="Other" @if ($bechelor_rooms->status == "Other")selected="selected" @endif >Other</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('status')) ? $errors->first('status') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Select Gender</label>
                  <input  type="radio" name="gender" value="male" @if($bechelor_rooms->gender=='male') checked @endif>Male
                  <input type="radio" name="gender" value="female" @if($bechelor_rooms->gender=='female') checked @endif>Female
                  <input type="radio" name="gender" value="other" @if($bechelor_rooms->gender=='other') checked @endif>Other
                  <p class="help-block">{{ ($errors->has('gender')) ? $errors->first('gender') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Select Religion</label>
                  <select name="religion" id="religion" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===Choose Religion===</option>
                    <option value="Islam" @if ($bechelor_rooms->religion == "Islam")selected="selected" @endif >Islam</option>
                    <option value="Hinduism" @if ($bechelor_rooms->religion == "Hinduism")selected="selected" @endif >Hinduism</option>
                    <option value="Buddhism" @if ($bechelor_rooms->religion == "Buddhism")selected="selected" @endif >Buddhism</option>
                    <option value="Christianity" @if ($bechelor_rooms->religion == "Christianity")selected="selected" @endif >Christianity</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('religion')) ? $errors->first('religion') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="married">Select Married</label>
                  <input type="radio" name="married" value="married" @if($bechelor_rooms->married=='married') checked @endif>Yes
                  <input type="radio" name="married" value="Unmarried"@if($bechelor_rooms->married=='Unmarried') checked @endif>No
                  <p class="help-block">{{ ($errors->has('married')) ? $errors->first('married') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>How Many Room Available</label>
                  <select name="room" id="room" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===Choose Room===</option>
                    <option value="1" @if ($bechelor_rooms->room == "1")selected="selected" @endif >1 Room</option>
                    <option value="2" @if ($bechelor_rooms->room == "2")selected="selected" @endif >2 Room</option>
                    <option value="3" @if ($bechelor_rooms->room == "3")selected="selected" @endif >3 Room</option>
                    <option value="4" @if ($bechelor_rooms->room == "4")selected="selected" @endif >4 Room</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('room')) ? $errors->first('room') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>How Many Seat Available</label>
                  <select name="seat" id="seat" class="form-control" required>
                    <option value="0" disabled="true" selected="true">===Choose Seat===</option>
                    <option value="1" @if ($bechelor_rooms->seat == "1")selected="selected" @endif >1 Seat</option>
                    <option value="2" @if ($bechelor_rooms->seat == "2")selected="selected" @endif >2 Seat</option>
                    <option value="3" @if ($bechelor_rooms->seat == "3")selected="selected" @endif >3 Seat</option>
                    <option value="4" @if ($bechelor_rooms->seat == "4")selected="selected" @endif >4 Seat</option>
                    <option value="5" @if ($bechelor_rooms->seat == "5")selected="selected" @endif >5 Seat</option>
                    <option value="6"@if ($bechelor_rooms->seat == "6")selected="selected" @endif >6 Seat</option>
                    <option value="7"@if ($bechelor_rooms->seat == "7")selected="selected" @endif >7 Seat</option>
                    <option value="8" @if ($bechelor_rooms->seat == "8")selected="selected" @endif >8 Seat</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('seat')) ? $errors->first('seat') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="room_type">Select Room Type</label>
                  <input type="radio" name="room_type" value="Semiphaka"@if($bechelor_rooms->room_type=='Semiphaka') checked @endif >Semiphaka
                  <input type="radio" name="room_type" value="Flat" @if($bechelor_rooms->room_type=='Flat') checked @endif>Flat
                  <p class="help-block">{{ ($errors->has('room_type')) ? $errors->first('room_type') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Per Seat Rent</label>
                  <input type="number" name="room_rent" class="form-control" value="{{ $bechelor_rooms->room_rent }}" required>
                  <p class="help-block">{{ ($errors->has('room_rent')) ? $errors->first('room_rent') : ''}}</p>
                </div>
                <div class="form-group">
                  <label>Seat Image</label>
                  <input type="file" name="image" value="{{ old('image')}}">
                  <p class="help-block">{{ ($errors->has('image')) ? $errors->first('image') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="facilities">Facilities</label></br>
                  <input type="checkbox" name="facilities[]" value="Fully Decorated" @if(in_array('Fully Decorated', $facilities)) checked @endif/>Fully Decorated</br>
                  <input type="checkbox" name="facilities[]" value="Attach Bathroom" @if(in_array('Attach Bathroom', $facilities)) checked @endif />Attach Bathroom</br>
                  <input type="checkbox" name="facilities[]" value="A Balcony along with the room" @if(in_array('A Balcony along with the room', $facilities)) checked @endif />A Balcony along with the room</br>
                  <input type="checkbox" name="facilities[]" value="24 Hours Water and Gass Supply" @if(in_array('24 Hours Water and Gass Supply', $facilities)) checked @endif />24 Hours Water and Gass Supply</br>
                  <input type="checkbox" name="facilities[]" value="A quiet and Lovely environment" @if(in_array('A quiet and Lovely environment', $facilities)) checked @endif />A quiet and Lovely environment</br>
                  <input type="checkbox" name="facilities[]" value="Wifi" @if(in_array('Wifi', $facilities)) checked @endif />Wifi</br>
                  <input type="checkbox" name="facilities[]" value="Freeze" @if(in_array('Freeze', $facilities)) checked @endif/>Freeze</br>
                  <input type="checkbox" name="facilities[]" value="Security Guard" @if(in_array('Security Guard', $facilities)) checked @endif/>Security Guard</br>
                  <input type="checkbox" name="facilities[]" value="Tiles" />Tiles</br>
                </div>
                <div class="form-group">
                  <label for="conditions">Conditions</label></br>
                  <input type="checkbox" name="conditions[]" value="Must be Nonsmoker" @if(in_array('Must be Nonsmoker', $conditions)) checked @endif />Must be Nonsmoker</br>
                  <input type="checkbox" name="conditions[]" value="Must be maintain of role of Room" @if(in_array('Must be maintain  of role of Room', $conditions)) checked @endif />Must be maintain  of role of Room</br>
                  <input type="checkbox" name="conditions[]" value="Before 11PM come back"@if(in_array('Before 11PM come back', $conditions)) checked @endif />Before 11PM come back</br>
                  
                </div>
                <div class="form-group">
                  <label>Mobile</label>
                  <input type="number" name="mobile" class="form-control" value="{{ $bechelor_rooms->mobile }}" required>
                  <p class="help-block">{{ ($errors->has('mobile')) ? $errors->first('mobile') : ''}}</p>
                </div>
                
                <div class="form-group">
                  <label for="city">Select City</label>
                  <select name="city" id="city" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose City==</option>
                    @foreach($cities as $city)
                    @if($city->id==$bechelor_rooms->city)
                    <option value="{{$city->id}}" selected>{{$city->city}}</option>
                    @else
                    <option value="{{$city->id}}">{{$city->city}}</option>
                    @endif
                    @endforeach
                  </select>
                  <p class="help-block">{{ ($errors->has('city')) ? $errors->first('city') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="thana">Select Thana</label>
                  <select name="thana" id="thana" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose Thana==</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('thana')) ? $errors->first('thana') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="ward">Select Ward</label>
                  <select name="ward" id="ward" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose Ward==</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('ward')) ? $errors->first('ward') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="address">Details Room Address</label>
                  <textarea name="address" id="address" class="form-control" cols="30" rows="5">{{$bechelor_rooms->address}}</textarea>
                  <p class="help-block">{{ ($errors->has('address')) ? $errors->first('address') : ''}}</p>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
              <script src="{{ url('')}}/assets/js/ajax.js"></script>
              <script type="text/javascript">
              $(document).ready(function(){
              $('select[name="city"]').on('change',function(){
              var cityid=$(this).val();
              if(cityid){
              $.ajax({
              url:'{{ url('')}}/cities/ajax/'+cityid,
              type:"GET",
              dataType:"json",
              success:function(data){
              $('select[name="thana"]').empty();
              $.each(data,function(key,value){
              $('select[name="thana"]').append('<option value="'+key+'">'+value+'</option>')
              });
              }
              });
              }else{
              $('select[name="thana"]').empty();
              }
              });
              $('select[name="thana"]').on('change',function(){
              var thanaid=$(this).val();
              console.log(thanaid);
              if(thanaid){
              $.ajax({
              url:'{{ url('')}}/thanas/ajax/'+thanaid,
              type:"GET",
              dataType:"json",
              success:function(data){
              $('select[name="ward"]').empty();
              $.each(data,function(key,value){
              $('select[name="ward"]').append('<option value="'+key+'">'+value+'</option>')
              });
              }
              });
              }else{
              $('select[name="ward"]').empty();
              }
              });
              });
              </script>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection