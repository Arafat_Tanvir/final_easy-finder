@extends('backend.admin.layouts.master')
@section('content')
<div class="container">
  <div class="card card-register mx-auto mt-5">
    <img src="{{asset('images/bechelor_room/bechelor_room.jpg')}}" height="120px">
      <h2>Bechelor Create Form <span class="pull-right"><a href="{{ route('bechelors.index')}}">Back</a></span></h2>
    <div class="card-body">
      <form method="POST" action="{{ route('bechelors.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="title">Title</label>
          <div class="form-input">
            <input type="text" class="form-control is-invalid form-control-sm" name="title" id="title" placeholder="3 Bechelor Roommate Wanted" value="" required>
            <div class="invalid-feedback">
              {{ ($errors->has('title')) ? $errors->first('title') : ''}}
            </div>
          </div>
        </div>
        
        <div class="form-group">
          <label for="month">Room Available Month</label>
          <div class="form-input">
            <select name="month" id="month" class="form-control is-invalid form-control-sm" required>
              <option value="0" disabled="true" selected="true">===Room Available From===</option>
              <option value="January">January</option>
              <option value="February">February </option>
              <option value="March"> March </option>
              <option value="April"> April </option>
              <option value="May"> May </option>
              <option value="June">June </option>
              <option value="July">July </option>
              <option value="August">August </option>
              <option value="September">September </option>
              <option value="October">October </option>
              <option value="November">November </option>
              <option value="December">December </option>
              
            </select>
            <div class="invalid-feedback">
              {{ ($errors->has('month')) ? $errors->first('month') : ''}}
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="status">Occupation</label>
          <div class="form-input">
            <select name="status" id="status" class="form-control is-invalid form-control-sm" required>
              <option value="0" disabled="true" selected="true">===Choose Status===</option>
              <option value="Student">Student</option>
              <option value="Job holder">Job Holder</option>
              <option value="Teacher">Teacher</option>
              <option value="No Conditions">No Conditions</option>
            </select>
            <div class="invalid-feedback">
              {{ ($errors->has('status')) ? $errors->first('status') : ''}}
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="gender">Gender</label>
          <div class="form-input">
            <input type="radio" name="gender" value="Male">Male
            <input type="radio" name="gender" value="Female">Female

            <div class="invalid-feedback">
              {{ ($errors->has('gender')) ? $errors->first('gender') : ''}}
            </div>
          </div>
        </div>


        <div class="form-group">
          <label for="religion">Religion</label>
          <div class="form-input">
            <select name="religion" id="religion" class="form-control is-invalid form-control-sm" required>
              <option value="0" disabled="true" selected="true">===Choose religion===</option>
              <option value="Hinduism" >Hinduism</option>
              <option value="Buddhism" >Buddhism</option>
              <option value="Christianity" >Christianity</option>
              <option value="Other" >Other</option>
            </select>
            <div class="invalid-feedback">
              {{ ($errors->has('religion')) ? $errors->first('religion') : ''}}
            </div>
          </div>
        </div>


        <div class="form-group">
          <label for="married_status">Married</label>
          <div class="form-input">
            <input type="radio" name="married_status" value="Married">Married</i>
            <input type="radio" name="married_status" value="Unmarried">Unmarried</i>
            <input type="radio" name="married_status" value="Married and Unmarried both">No Conditions</i>
            <div class="invalid-feedback">
              {{ ($errors->has('name')) ? $errors->first('name') : ''}}
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="category_id">Select Category </label>
          <div class="input-group">
            <select name="category_id" id="category_id" class="form-control is-invalid form-control-sm">
              <option value="0" disabled="true" selected="true">===Choose Category==</option>
              @foreach($categories as $category)
              <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
             <div class="invalid-feedback">
              {{ ($errors->has('category_id')) ? $errors->first('category_id') : ''}}
            </div>
          </div>





        <div class="form-group">
          <label for="seat">Available Seat</label>
          <div class="form-input">
            <input type="number" class="form-control is-invalid form-control-sm input-md" step="0.01" name="seat" id="seat" placeholder="How Many seat Available" value="">
            <div class="valid-feedback">
              {{ ($errors->has('seat')) ? $errors->first('seat') : ''}}
            </div>
          </div>
        </div>


        <div class="form-group">
          <label for="room_type">Room Type</label>
          <div class="form-input">
            <select name="room_type" id="room_type" class="form-control is-invalid form-control-sm input-md" required>
              <option value="0" disabled="true" selected="true">===Room Type===</option>
              <option value="Flat">Flat</option>
              <option value="Semi-ripe house">Semi-ripe house</option>
            </select>
            <div class="invalid-feedback">
              {{ ($errors->has('room_type')) ? $errors->first('room_type') : ''}}
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="room_rent">Room rent</label>
          <div class="form-input">
            <input type="number" class="form-control is-invalid form-control-sm input-md" step="0.01" name="room_rent" id="room_rent" placeholder="Enter Ward room_rent" value="">
            <div class="invalid-feedback">
              {{ ($errors->has('room_rent')) ? $errors->first('room_rent') : ''}}
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="facilities">Facilities</label><br>
          <input type="checkbox" name="facilities[]" value="Fully Decorated"/>Fully Decorated</br>
          <input type="checkbox" name="facilities[]" value="Attach Bathroom" />Attach Bathroom</br>
          <input type="checkbox" name="facilities[]" value="A Balcony along with the room" />A Balcony along with the room</br>
          <input type="checkbox" name="facilities[]" value="24 Hours Water and Gass Supply" />24 Hours Water and Gass Supply</br>
          <input type="checkbox" name="facilities[]" value="A quiet and Lovely environment" />A quiet and Lovely environment</br>
          <input type="checkbox" name="facilities[]" value="Wifi" />Wifi</br>
          <input type="checkbox" name="facilities[]" value="Freeze"/>Freeze</br>
          <input type="checkbox" name="facilities[]" value="Security Guard"/>Security Guard</br>
          <input type="checkbox" name="facilities[]" value="Tiles" />Tiles</br>
        </div>
        <div class="form-group">
          <label for="conditions">Conditions</label><br>
          
          <input type="checkbox" name="conditions[]" value="Must be Nonsmoker" />Must be Nonsmoker</br>
          <input type="checkbox" name="conditions[]" value="Must be maintain of role of Room" />Must be maintain of role of Room</br>
          <input type="checkbox" name="conditions[]" value="Before 11PM come back" />Before 11PM come back</br>
          
        </div>
        <div class="form-group">
          <label for="mobile">Mobile</label>
          <div class="form-input">
            <input type="number" class="form-control is-invalid form-control-sm" name="mobile" id="mobile" placeholder="Enter mobile" value="">
            <div class="invalid-feedback">
              {{ ($errors->has('mobile')) ? $errors->first('mobile') : ''}}
            </div>
            <p class="help-block">{{ ($errors->has('mobile')) ? $errors->first('mobile') : ''}}</p>
          </div>
        </div>
        
        <div class="form-group">
          <label for="city_id">Select City</label>
          <div class="input-group">
            <select name="city_id" id="city_id" class="form-control is-invalid form-control-sm">
              <option value="0" disabled="true" selected="true">===Choose City==</option>
              @foreach($cities as $city)
              <option value="{{$city->id}}">{{$city->name}}</option>
              @endforeach
            </select>
             <div class="invalid-feedback">
              {{ ($errors->has('city_id')) ? $errors->first('city_id') : ''}}
            </div>
          </div>
          <div class="form-group">
            <label for="thana_id">Select Thana</label>
            <div class="input-group">
              <select name="thana_id" id="thana_id" class="form-control is-invalid form-control-sm">
                <option value="0" disabled="true" selected="true">===Choose Thana==</option>
              </select>
              <div class="invalid-feedback">
              {{ ($errors->has('thana_id')) ? $errors->first('thana_id') : ''}}
            </div>
            </div>
            <div class="form-group">
              <label for="ward_id">Select Ward</label>
              <div class="input-group">
                <select name="ward_id" id="ward_id"  class="form-control is-invalid form-control-sm">
                  <option value="0" disabled="true" selected="true">===Choose ward==</option>
                </select>
                <div class="invalid-feedback">
              {{ ($errors->has('ward_id')) ? $errors->first('ward_id') : ''}}
            </div>
              </div>
              <div class="form-group">
                <label for="address">Details Room Address</label>
                <div class="input-group">
                  <textarea name="address" id="address"  class="form-control is-invalid form-control-sm" cols="30" rows="5"></textarea>
                  <div class="invalid-feedback">
              {{ ($errors->has('address')) ? $errors->first('address') : ''}}
            </div>
                </div>

                <div class="form-group">
                    <label>Image</label>
                    <div class="row">
                        <div class="col-sm-4" style="margin-top: 5px">
                            <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                        </div>
                        <div class="col-sm-4" style="margin-top: 5px">
                            <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                        </div>
                        <div class="col-sm-4" style="margin-top: 5px">
                            <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                        </div>
                        <div class="col-sm-4" style="margin-top: 5px">
                            <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                        </div>
                        <div class="col-sm-4" style="margin-top: 5px">
                            <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                        </div>
                        <div class="col-sm-4" style="margin-top: 5px">
                            <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
                        </div>
                        <div class="invalid-feedback">
              {{ ($errors->has('address')) ? $errors->first('address') : ''}}
            </div>
                    </div>
                </div>
                
                <button class="btn btn-primary mt-2" type="submit">Create</button>
              </form>
            </div>
          </div>
          
        </div>
        @endsection

        @section('scripts')

        <script type="text/javascript">
                $(document).ready(function(){
                  $('select[name="city_id"]').on('change',function(){
                    var city_id=$(this).val();
                    //alert(city_id);
                    if(city_id){
                      $.ajax({
                        url:'{{ url('')}}/cities/ajax/'+city_id,
                        type:"GET",
                        dataType:"json",
                        success:function(data){
                          $('select[name="thana_id"]').empty();
                          $.each(data,function(key,value){
                          $('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
                          });
                        }
                      });
                    }else{
                      $('select[name="thana_id"]').empty();
                      }
                  });
                  $('select[name="thana_id"]').on('change',function(){
                    var thana_id=$(this).val();
                    //alert(thana_id);
                    console.log(thana_id);
                    if(thana_id){
                      $.ajax({
                        url:'{{ url('')}}/thanas/ajax/'+thana_id,
                        type:"GET",
                        dataType:"json",
                        success:function(data){
                        $('select[name="ward_id"]').empty();
                          $.each(data,function(key,value){
                          $('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
                          });
                        }
                      });
                    }else{
                      $('select[name="ward_id"]').empty();
                    }
                });
              });
              </script>

        @endsection

