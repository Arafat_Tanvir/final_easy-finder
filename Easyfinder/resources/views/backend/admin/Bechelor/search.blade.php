@extends('backend.admin.layouts.master')
@section('content')
<div class="container">
  <div class="card card-register mx-auto mt-5">
    <!-- <img src="{{asset('images/bechelor_room/bechelor_room.jpg')}}" height="120px"> -->
    <h2>Bechelor Create Form <span class="pull-right"><a href="{{ route('bechelors.index')}}">Back</a></span></h2>
    <div class="card-body">
      <form role="form" action="{{ route('search_post.search_post')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field()}}

        <div class="form-group">
          <label for="category_id">Select Categories</label>
          <div class="input-group">
            <select name="category_id" id="category_id" class="form-control is-invalid form-control-sm">
              <option value="0" disabled="true" selected="true">===Choose Categories==</option>
              @foreach($categories as $category)
              <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
            <div class="invalid-feedback">
              {{ ($errors->has('category_id')) ? $errors->first('category_id') : ''}}
            </div>
          </div>
        </div>
        
        <div class="form-group">
          <label for="category">Select City</label>
          <div class="input-group">
            <select name="city_id" id="city_id" class="form-control is-invalid form-control-sm">
              <option value="0" disabled="true" selected="true">===Choose City==</option>
              @foreach($cities as $city)
              <option value="{{$city->id}}">{{$city->name}}</option>
              @endforeach
            </select>
            <div class="invalid-feedback">
              {{ ($errors->has('city_id')) ? $errors->first('city_id') : ''}}
            </div>
          </div>
        </div>
          <div class="form-group">
            <label for="thana_id">Select Thana</label>
            <div class="input-group">
              <select name="thana_id" id="thana_id" class="form-control is-invalid form-control-sm">
                <option value="0" disabled="true" selected="true">===Choose Thana==</option>
              </select>
              <div class="invalid-feedback">
                {{ ($errors->has('thana_id')) ? $errors->first('thana_id') : ''}}
              </div>
            </div>
          </div>
            <div class="form-group">
              <label for="ward_id">Select Ward</label>
              <div class="input-group">
                <select name="ward_id" id="ward_id"  class="form-control is-invalid form-control-sm">
                  <option value="0" disabled="true" selected="true">===Choose ward==</option>
                </select>
                <div class="invalid-feedback">
                  {{ ($errors->has('ward_id')) ? $errors->first('ward_id') : ''}}
                </div>
              </div>
            </div>
              <button type="submit" class="btn btn-primary">Search</button>
            </form>
          </div>
        </div>
      </div>
      @endsection
      @section('scripts')
      <script type="text/javascript">
      $(document).ready(function(){
      $('select[name="city_id"]').on('change',function(){
      var city_id=$(this).val();
      //alert(city_id);
      if(city_id){
      $.ajax({
      url:'{{ url('')}}/cities/ajax/'+city_id,
      type:"GET",
      dataType:"json",
      success:function(data){
      $('select[name="thana_id"]').empty();
      $.each(data,function(key,value){
      $('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
      });
      }
      });
      }else{
      $('select[name="thana_id"]').empty();
      }
      });
      $('select[name="thana_id"]').on('change',function(){
      var thana_id=$(this).val();
      //alert(thana_id);
      console.log(thana_id);
      if(thana_id){
      $.ajax({
      url:'{{ url('')}}/thanas/ajax/'+thana_id,
      type:"GET",
      dataType:"json",
      success:function(data){
      $('select[name="ward_id"]').empty();
      $.each(data,function(key,value){
      $('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
      });
      }
      });
      }else{
      $('select[name="ward_id"]').empty();
      }
      });
      });
      </script>
      @endsection