<div class="card-header" mt-2 style="background-color: pink">
<form class="form-inline" action="{{ route('cards.store', $bechelor->id)}}" method="POST">
	{{csrf_field()}}

	<input type="hidden" name="bechelor_id" value="{{$bechelor->id}}">

	<div class="form-group">
          <label for="seat" style="margin-left: 20px">How Many Seat Do You Booking?</label>
          <div class="form-input" style="margin-left: 20px">
            <input type="number" class="form-control is-valid form-control-sm input-md" name="seat" id="seat" placeholder="" value="">
            <div class="valid-feedback">
              {{ ($errors->has('seat')) ? $errors->first('seat') : ''}}
            </div>
          </div>
        </div>
        <hr>
	<button type="submit" class="badge badge-pill badge-primary"><i class="fa fa-plus">Card To Booking</i></button>
</form>
</div>