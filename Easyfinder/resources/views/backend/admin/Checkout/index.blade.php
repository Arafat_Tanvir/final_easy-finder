@extends('backend.admin.layouts.master')
@section('content')
<div class="container">
	<div class="card mx-auto mt-5">
		<div class="card">
			<div class="card-header">
				<p class="small text-center text-muted mt-2">
					<img src="{{asset('images/bechelor_room/bechelor_room.jpg')}}" height="80px" width="100%">
				</p>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-4">
						<strong class="badge badge-success">Room Details</strong>
						<div class="badge badge-fill badge-light mt-2" style="text-align: left;">
							@foreach(App\Backend\Card::total_Cards() as $card)
							<h6>
							Title : <strong>{{ $card->bechelor->title}}</strong>
							</h6>
							
							<h6>
							Seat Request : <strong>{{$card->seat}}</strong>
							</h6>
						
							<h6>
							Room Rent : <strong>{{ $card->bechelor->room_rent}}</strong>
							</h6>

							@endforeach
						</div>
						<a href="{{route('cards.index')}}" class="badge badge-fill badge-warning mt-2">Checked Room</a>
						<hr class="mt-5 mb-5">
						<strong class="badge badge-success">Money Card Details</strong>
						<div class="badge badge-fill badge-light mt-2" style="text-align: left; color: black">
						    @php
						        $total_service=0;

							    $total_price=0;

							@endphp

							@foreach(App\Backend\Card::total_Cards() as $card)

								@php

								    $total_price+=$card->bechelor->room_rent * $card->seat

								@endphp

								@php
								  $total_service=$card->seat * App\Backend\Setting::first()->service_charge 
								@endphp

								<p>Price : <strong>{{ $card->bechelor->room_rent * $card->seat }}</strong></p>
							@endforeach
							<p> Total Price  with Service cost : <strong>{{ $total_price + $total_service  }}</strong></p>
										
						</div>
					</div>
					<div class="col-lg-8">
						<form class="form-horizontal" method="POST" action="{{ route('orders.store')}}" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="row ">
								<div class="col-sm-6 badge badge-fill badge-light" style="text-align: left;">
									<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
										
										<div class="col-md-12">
											<label for="name" class="control-label">Name</label>
											<div class="input-group">
												<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
												<input id="name" type="text" class="form-control" name="name" value="{{ Auth::check() ? Auth::user()->name : '' }}" placeholder="Enter Your name" autofocus>
												@if ($errors->has('name'))
												<span class="help-block">
													<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>
									<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
										
										<div class="col-md-12">
											<label for="email" class="control-label">Email</label>
											<div class="input-group">
												<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
												<input id="email" type="email" class="form-control" name="email" value="{{ Auth::check() ? Auth::user()->email :''}}" placeholder="Enter Your email" autofocus>
												@if ($errors->has('email'))
												<span class="help-block">
													<strong>{{ $errors->first('email') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>

									<div class="form-group{{ $errors->has('street_address') ? ' has-error' : '' }}">
										
										<div class="col-md-12">
											<label for="street_address" class="control-label">Street Address</label>
											<div class="input-group">
												<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
												<textarea name="street_address" id="street_address" class="form-control" cols="30" rows="5" placeholder="Enter Your message"  autofocus>{{ Auth::check() ? Auth::user()->street_address :''}}</textarea>
												@if ($errors->has('street_address'))
												<span class="help-block">
													<strong>{{ $errors->first('street_address') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>

									<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
										
										<div class="col-md-12">
											<label for="phone" class="control-label">Phone</label>
											<div class="input-group">
												<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
												<input id="phone" type="phone" class="form-control" name="phone" value="{{ Auth::check() ? Auth::user()->phone :''}}" placeholder="Enter Your phone" autofocus>
												@if ($errors->has('phone'))
												<span class="help-block">
													<strong>{{ $errors->first('phone') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>

									<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
										
										<div class="col-md-12">
											<label for="message" class="control-label">Message</label>
											<div class="input-group">
												<div class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></div>
												<textarea name="message" id="message" class="form-control" cols="30" rows="5" value="{{ old('message') }}"placeholder="Enter Your message"  autofocus></textarea>
												@if ($errors->has('message'))
												<span class="help-block">
													<strong>{{ $errors->first('message') }}</strong>
												</span>
												@endif
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group{{ $errors->has('payment_method_id') ? ' has-error' : '' }}">
										
										<div class="col-md-12" style="margin-bottom: 20px">
											<label for="payment_method_id" class="control-label">Payment Method</label>
											<div class="input-group">
												<select name="payment_method_id" class="form-control" value="{{old('payment_method_id')}}" id="payments">
													<option value="0" disabled="true" selected="true">===Select Payment===</option>
													@foreach($payments as $payment)
													<option value="{{$payment->short_name}}">{{$payment->name}}</option>
													@endforeach
												</select>
											</div>
											@if ($errors->has('payment_method_id'))
											<span class="help-block badge badge-danger" >
												<strong>{{ $errors->first('payment_method_id') }}</strong>
											</span>
											@endif
										</div>
										@foreach($payments as $payment)
										<div class="margin-top-20">
											@if($payment->short_name=="Cash")
											<div id="payment_{{ $payment->short_name}}" class="hidden alert alert-success">
												<div class="cash">
													<div class="badge badge-success">
														<img src="{{asset('images/payments/'.$payment->image)}}" alt="Cash Image" class="img-fluid img-thumbnail" >
													</div>
													<hr>
													<h4 class="badge badge-warning"> {{$payment->name}} Payment </h4><br>
													<h5 class="alert alert-info">For Cash in there is noting necssary.Just Finish Your Order</h5><br>
													<div class="alert alert-danger">
														<small>You Will get Your Products in Two or Three bussiness Days </small>
													</div>
												</div>
											</div>
											@else
											<div id="payment_{{ $payment->short_name}}" class="hidden alert alert-success">
												<div class="bikash">
													<div class="badge badge-success">
														<img src="{{asset('images/payments/'.$payment->image)}}" alt="Rocket Image" class=" img-fluid img-thumbnail" height="220px">
													</div>
													<hr>
													<h4 class="badge badge-warning"> {{$payment->name}} Payment </h4>
													<p>
														<strong>{{$payment->name}} No: {{ $payment->no}}</strong><br>
														<strong class="badge badge-info"> Account Type: {{ $payment->type}}</strong>
														<div class="alert alert-danger">
															<p>
																Please send the Above money to this <span badge badge-success>{{$payment->name}}</span> money and write your transaction code bellow  there
															</p>
														</div>
													</p>
												</div>
											</div>
											@endif
										</div>
										@endforeach
										<input type="text" id="transaction_id" placeholder="Please inter Transaction ID" name="transaction_id" class="form-control hidden">
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary" style="width: 100%">
							Order
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$("#payments").change(function() {
		//alert("dfkas");
		$payment_method=$("#payments").val();
		if ($payment_method=="Cash") {
			$("#payment_Cash").removeClass('hidden');
			$("#payment_Rocket").addClass('hidden');
			$("#payment_Bikash").addClass('hidden');
		}else if($payment_method=="Bikash"){
			$("#payment_Bikash").removeClass('hidden');
			$("#transaction_id").removeClass('hidden');
			$("#payment_Cash").addClass('hidden');
			$("#payment_Rocket").addClass('hidden');
		}else if($payment_method=="Rocket"){
			$("#payment_Rocket").removeClass('hidden');
			$("#transaction_id").removeClass('hidden');
			$("#payment_Cash").addClass('hidden');
			$("#payment_Bikash").addClass('hidden');
		}
	});
</script>
@endsection