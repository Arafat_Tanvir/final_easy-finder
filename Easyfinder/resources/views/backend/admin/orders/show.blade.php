@extends('backend.admin.layouts.master')
@section('content')
 <div class="container">
    <div class="card mx-auto mt-5">
        <div class="card">
            <div class="card-header">
                <p class="small text-center text-muted mt-2">
                    <img src="{{asset('images/bechelor_room/bechelor_room.jpg')}}" height="80px" width="100%">
                </p>
            </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-8">
                   <h6>Order Name:  {{ $orders->name }} </h6>
                   <h6>Order Email:  {{ $orders->email}} </h6>
                   <h6>Order Phone {{ $orders->phone }} </h6>
                   <h6>Order Shipping Address {{  $orders->street_address }} </h6>
              </div>
              <div class="col-sm-4">
                <span>Transaction Name=>{{$orders->payment->name}}</span>
                <span> Transaction Id=>{{ $orders->transaction_id}}</span>
              </div>
            </div>
          </div>
     	</div>
        <div class="container">
           <div class="card mx-auto mt-2">
            <div class="card-header">
        			@if($orders->cards->count() > 0)
        			<table class="table table-bordered">
        				<thead>
        					<tr>
        						<th>SL</th>
        						<th>bechelor Title</th>
        						<th>Image</th>
        						<th>Seat</th>
        						<th>Price</th>
        						<th>Total Price</th>
        						<th>Action</th>
        					</tr>
        				</thead>
        				<tbody>
        					<tr>
        						@php

        						$total_price=0;

        						@endphp
        						<td style="display: none;">{{$a=1}}</td>
        						@foreach($orders->cards as $card)
        						<td>{{ $a++ }}</td>
        						<td>
        							<a href="{{route('bechelors.show',$card->bechelor->room_rent)}}">{{ $card->bechelor->title}}</a>
        						</td>
        						<td>
        						    <div id="demo" class="carousel slide" data-ride="carousel">
        								<!-- The slideshow -->
        								<div class="carousel-inner">
        									<div class="carousel-inner">
        										@php
        										$i=1
        										@endphp
        										@foreach($card->bechelor->images as $image)
        										<div style="text-align: center;" class="carousel-item {{$i==1 ? 'active' : ''}}">
        											<img class="img-fluid img-thumbnail" src="{{asset('images/bechelor_room/'.$image->images)}}" alt="" >
        										</div>
        										@php
        										$i++
        										@endphp
        										@endforeach
        									</div>
        								</div>
        								<!-- Left and right controls -->
        								<a class="carousel-control-prev" href="#demo" data-slide="prev">
        									<span class="carousel-control-prev-icon"></span>
        								</a>
        								<a class="carousel-control-next" href="#demo" data-slide="next">
        									<span class="carousel-control-next-icon"></span>
        								</a>
        							</div>
        						</td>
        						<td>
        							<form style="margin-left: 40px" class="form-inline" action="{{ route('cards.update', $card->id)}}" method="POST">
        								{{csrf_field()}}
        									<input type="number" value="{{$card->seat}}" name="seat" class="col-sm-6 form-control">
        							        <button type="submit" class="btn btn-outline-primary btn-sm">Update</button>
        							</form>
        						</td>
        						<td>{{$card->bechelor->price}} Taka</td>
        						<td>{{$card->bechelor->price * $card->seat}} Taka</td>
        						<td>
        							<a href="#DeleteModal{{ $card->id}}" data-toggle="modal" class="btn btn-outline-danger btn-sm">Delete</a>
        								<div class="modal fade" id="DeleteModal{{$card->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        									<div class="modal-dialog" role="document">
        										<div class="modal-content">
        											<div class="modal-header">
        												<h5 class="modal-title" id="exampleModalLabel">Are You Sure To Delete!</h5>
        												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        												<span aria-hidden="true">&times;</span>
        												</button>
        											</div>
        											<div class="modal-body">
        												<form action="{{ route('cards.destroy', $card->id)}}" method="POST">
        													{{csrf_field()}}
        												<button type="submit" class="btn btn-outline-primary btn-lg">Delete</button>
        												</form>
        											</div>
        											<div class="modal-footer">
        												<button type="button" class="btn btn-outline-success btn-lg" data-dismiss="modal">Close</button>
        											</div>
        										</div>
        									</div>
        								</div>

        							</a>
        						</td>
        					</tr>
        					@php

        					$total_price+=$card->bechelor->price * $card->seat

        					@endphp
        					@endforeach
        					<tr>
        						<th style="margin-left: 120px" colspan="5">Total Amount</th>
        						<th colspan="2">{{ $total_price }} Taka</th>
        					</tr>
        				</tbody>
        			</table>
        			<div class="float-right">
        			<a href="{{ route('bechelors.index')}}" class="btn btn-outline-info btn-lg">Bechelor Room</a>
        			
        			</div>
        			@else
        			<p class="badge badge-success" style="text-align: center; font-size: 20px">
        				No Iteam in your Card
        			</p>
        			<br>
        			<a href="{{ route('bechelors.index')}}" class="btn btn-outline-info btn-lg margin-top-20">Bechelor Room</a>
        		    @endif
        		</div>
        	</div>
        </div>
     	</div>

      <form class="form-inline" action="{{ route('orders.paid', $orders->id)}}" method="POST">
        {{csrf_field()}}
        @if($orders->is_paid)
              <button type="submit" class="btn btn-outline-primary btn-sm">Paid Order</button>
        @else
              <button type="submit" class="btn btn-outline-primary btn-sm">Cencel Paid</button>
        @endif
      </form>
      <hr>
      <form class="form-inline" action="{{ route('orders.complete', $orders->id)}}" method="POST">
        {{csrf_field()}}
        @if($orders->is_complete)
              <button type="submit" class="btn btn-outline-primary btn-sm">Complete Order</button>
        @else
              <button type="submit" class="btn btn-outline-primary btn-sm">Cencel Order</button>
        @endif
      </form>
     </div>
        @endsection
