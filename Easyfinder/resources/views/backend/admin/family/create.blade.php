@extends('backend.admin.layouts.master')
@section('content')
<!-- <img src="{{asset('images/bechelor_room/bechelor_room.jpg')}}" height="120px" width="100%"> -->
<div class="col-sm-10 col-sm-offset-1">
    
    {{ Form::open([
    'route' => 'families-store',
    'method' => 'POST',
    'class'=>'form-horizontal form-horizontal row-fluid',
    'enctype'=>'multipart/form-data'
    ])
    }}
    <div class="form-group" style="margin-top: 20px;">
        <label for="category_id" class="col-sm-4">{{ __('Room Category') }}</label>
        <div class="col-sm-8">
            <select name="category_id" type="category_id" id="category_id" class="form-control{{ $errors->has('category_id') ? ' is-valid' : '' }}" value="{{ old('category_id') }}">
                <option value="0" disabled="true" selected="true">===Select Category===</option>
                @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('category_id'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="title" class="col-sm-4">{{ __('Title') }}</label>
        <div class="col-sm-8">
            <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-valid' : '' }}" name="title" value="{{ old('title') }}">
            @if ($errors->has('title'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="room_status" class="col-sm-4">{{ __('Room Status') }}</label>
        <div class="col-sm-8">
            <select name="room_status" type="room_status" id="room_status" class="form-control{{ $errors->has('room_status') ? ' is-valid' : '' }}" value="{{ old('room_status') }}">
                <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                <option value="Flat">Flat</option>
                <option value="Semi-ripe house">Semi-ripe house</option>
            </select>
            @if ($errors->has('room_status'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('room_status') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="month" class="col-sm-4">{{ __('Month') }}</label>
        <div class="col-sm-8">
            <select name="month" type="month" id="month" class="form-control{{ $errors->has('month') ? ' is-valid' : '' }}" value="{{ old('month') }}">
                <option value="0" disabled="true" selected="true">===Room Available From===</option>
                <option value="January">January</option>
                <option value="February">February </option>
                <option value="March"> March </option>
                <option value="April"> April </option>
                <option value="May"> May </option>
                <option value="June">June </option>
                <option value="July">July </option>
                <option value="August">August </option>
                <option value="September">September </option>
                <option value="October">October </option>
                <option value="November">November </option>
                <option value="December">December</option>
                <option value="No Condition">No Condition</option>
            </select>
            @if ($errors->has('month'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('month') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="religion" class="col-sm-4">{{ __('Religion') }}</label>
        <div class="col-sm-8">
            <select name="religion" type="religion" id="religion" class="form-control{{ $errors->has('religion') ? ' is-valid' : '' }}" value="{{ old('religion') }}">
                <option value="0" disabled="true" selected="true">===Family Religion===</option>
                <option value="Islam" >Islam</option>
                <option value="Hinduism" >Hinduism</option>
                <option value="Buddhism" >Buddhism</option>
                <option value="Christianity" >Christianity</option>
                <option value="No Condition">No Condition</option>
            </select>
            @if ($errors->has('religion'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('religion') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="total_room" class="col-sm-4">{{ __('Total Room') }}</label>
        <div class="col-sm-8">
            <select name="total_room" type="total_room" id="total_room" class="form-control{{ $errors->has('total_room') ? ' is-valid' : '' }}" value="{{ old('total_room') }}">
                <option value="0" disabled="true" selected="true">===Family Total Room===</option>
                <option value="1" >One</option>
                <option value="2" >Two</option>
                <option value="3" >Three</option>
                <option value="4" >Four</option>
                <option value="5">Five</option>
                <option value="6" >Six</option>
                <option value="7" >Seven</option>
                <option value="8" >Eight</option>
            </select>
            @if ($errors->has('total_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('total_room') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="bed_room" class="col-sm-4">{{ __('How Many Bed Room') }}</label>
        <div class="col-sm-8">
            <select name="bed_room" type="bed_room" id="bed_room" class="form-control{{ $errors->has('bed_room') ? ' is-valid' : '' }}" value="{{ old('bed_room') }}">
                <option value="0" disabled="true" selected="true">===Select Bed Room Available===</option>
                <option value="1" >One</option>
                <option value="2" >Two</option>
                <option value="3" >Three</option>
                <option value="4" >Four</option>
                <option value="5">Five</option>
            </select>
            @if ($errors->has('bed_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('bed_room') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="bath_room" class="col-sm-4">{{ __('How Many Bath Room') }}</label>
        <div class="col-sm-8">
            <select name="bath_room" type="bath_room" id="bath_room" class="form-control{{ $errors->has('bath_room') ? ' is-valid' : '' }}" value="{{ old('bath_room') }}">
                <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                <option value="1" >One</option>
                <option value="2" >Two</option>
                <option value="3" >Three</option>
                <option value="4" >Four</option>
                <option value="5">Five</option>
            </select>
            @if ($errors->has('bath_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('bath_room') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="dinning_room" class="col-sm-4">{{ __('Dinning Room') }}</label>
        <div class="col-sm-8">

            <div class="row">
                <label class="col-sm-2 col-sm-offset-2">
                    <input type="radio" name="dinning_room" value="Dinning room">Yes
                </label>
                <label class="col-sm-2 col-sm-offset-2">
                     <input type="radio" name="dinning_room" value="No Dinning room">No
                </label>
            </div>


            @if ($errors->has('dinning_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('dinning_room') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label for="room_size" class="col-sm-4">{{ __('Room Size') }}</label>
        <div class="col-sm-8">
            <input id="room_size" type="number" class="form-control{{ $errors->has('room_size') ? ' is-valid' : '' }}" name="room_size" value="{{ old('room_size') }}">
            @if ($errors->has('room_size'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('room_size') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="drawing_room" class="col-sm-4">{{ __('Drawing Room') }}</label>
        <div class="col-sm-8">
            <div class="row">
                <label class="col-sm-2 col-sm-offset-2">
                    <input type="radio" name="drawing_room" value="Dinning room">Yes
                </label>
                <label class="col-sm-2 col-sm-offset-2">
                    <input type="radio" name="drawing_room" value="No Dinning room">No
                </label>
            </div>
            
            @if ($errors->has('drawing_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('drawing_room') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="room_rent" class="col-sm-4">{{ __('Room Rent') }}</label>
        <div class="col-sm-8">
            <input id="room_rent" type="number" class="form-control{{ $errors->has('room_rent') ? ' is-valid' : '' }}" name="room_rent" value="{{ old('room_rent') }}">
            @if ($errors->has('room_rent'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('room_rent') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-sm-4">{{ __('Short Description') }}</label>
        <div class="col-sm-8">
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-valid' : '' }}" name="description" value="{{ old('description') }}" cols="40" rows="5">{{ old('description') }}</textarea>
            @if ($errors->has('description'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="facilities" class="col-sm-4">{{ __('Room facilities') }}</label>
        <div class="col-sm-8">
            <label class="checkbox-inline" >
                <input type="checkbox" name="facilities[]" value="Fully Decorated"/>Fully Decorated
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="Attach Bathroom" />Attach Bathroom
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="A Balcony along with the room" />A Balcony along
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="24 Hours Water and Gass Supply" />24 Hours Water  & guss
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="A quiet and Lovely environment" />A quiet and Lovely environment
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="Wifi" />Wifi
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="Security Guard"/>Security Guard</br>
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="Tiles" />Tiles</br>
            </label>
            <br>
            @if ($errors->has('facilities'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('facilities') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="conditions" class="col-sm-4">{{ __('Room Conditions') }}</label>
        <div class="col-sm-8 checkbox-inline">
            <label class="checkbox-inline">
                <input type="checkbox" name="conditions[]" value="Must be Nonsmoker" />Must be Nonsmoker
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="conditions[]" value="Must be maintain of role of Room" />Must be maintain of role of Room
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="conditions[]" value="Before 11PM come back" />Before 11PM come back
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="conditions[]" value="Before 11PM come back" />Before 11PM come back</br>
            </label>
            <br>
            @if ($errors->has('conditions'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('conditions') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="mobile" class="col-sm-4">{{ __('Phone Number') }}</label>
        <div class="col-sm-8">
            <input id="mobile" type="number" class="form-control{{ $errors->has('mobile') ? ' is-valid' : '' }}" name="mobile" value="{{ old('mobile') }}">
            @if ($errors->has('mobile'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('mobile') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="city_id" class="col-sm-4">{{ __(' City Name') }}</label>
        <div class="col-sm-8">
            <select name="city_id" type="text" id="city_id" class="form-control{{ $errors->has('city_id') ? ' is-valid' : '' }}" value="{{ old('city_id') }}">
                <option value="0" disabled="true" selected="true">===Select City===</option>
                @foreach($cities as $city)
                <option value="{{$city->id}}">{{$city->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('city_id'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="thana_id" class="col-sm-4">{{ __(' Thana Name') }}</label>
        <div class="col-sm-8">
            <select name="thana_id" type="text" id="thana_id" class="form-control{{ $errors->has('thana_id') ? ' is-valid' : '' }}" value="{{ old('thana_id') }}">
                <option value="0" disabled="true" selected="true">===Select Thana===</option>
            </select>
            @if ($errors->has('thana_id'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('thana_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="ward_id" class="col-sm-4">{{ __(' Ward Name') }}</label>
        <div class="col-sm-8">
            <select name="ward_id" type="text" id="ward_id" class="form-control{{ $errors->has('ward_id') ? ' is-valid' : '' }}" value="{{ old('ward_id') }}">
                <option value="0" disabled="true" selected="true">===Select Ward===</option>
            </select>
            @if ($errors->has('ward_id'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('ward_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-4">{{ __('Address') }}</label>
        <div class="col-sm-8">
            <textarea id="address" class="form-control{{ $errors->has('address') ? ' is-valid' : '' }}" name="address" value="{{ old('address') }}" cols="40" rows="5">{{ old('address') }}</textarea>
            @if ($errors->has('address'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="images" class="col-sm-4">{{ __('images') }}</label>
        <div class="col-sm-8">

           <div class="col-sm-4" style="margin-top: 5px">
                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
            </div>

            <div class="col-sm-4" style="margin-top: 5px">
                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
            </div>

            <div class="col-sm-4" style="margin-top: 5px">
                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
            </div>

            <div class="col-sm-4" style="margin-top: 5px">
                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
            </div>

            <div class="col-sm-4" style="margin-top: 5px">
                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
            </div>

            <div class="col-sm-4" style="margin-top: 5px">
                <input type="file" class="" name="images[]" value="{{ old('images')}}" multiple>
            </div>

            @if ($errors->has('images'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('images') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-success" type="submit" style="text-align: center;">Create Post For Family</button>
    </div>
    
    {{ Form::close() }}
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
$('select[name="city_id"]').on('change',function(){
var city_id=$(this).val();
//alert(city_id);
if(city_id){
$.ajax({
url:'{{ url('')}}/cities/ajax/'+city_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="thana_id"]').empty();
$.each(data,function(key,value){
$('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="thana_id"]').empty();
}
});
$('select[name="thana_id"]').on('change',function(){
var thana_id=$(this).val();
//alert(thana_id);
console.log(thana_id);
if(thana_id){
$.ajax({
url:'{{ url('')}}/thanas/ajax/'+thana_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="ward_id"]').empty();
$.each(data,function(key,value){
$('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="ward_id"]').empty();
}
});
});
</script>
@endsection