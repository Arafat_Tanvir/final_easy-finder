@extends('backend.admin.layouts.master')
@section('content')

@foreach($families as $family)
<div class="all-content-wrapper" style="margin-bottom: 200px">
        <div class="single-product-tab-area mg-t-15 mg-b-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                        <div id="myTabContent1" class="tab-content">
                            @php
                                $i=1
                            @endphp


                            @foreach($family->images as $image)


                            @if($i==1)
                            <div class="product-tab-list tab-pane fade active in" id="single-tab1">
                                <img src="{{asset('images/family_room/'.$image->images)}}" alt="" class="img-fluid img-thumbnail" />
                            </div>
                            @elseif($i==2)
                            <div class="product-tab-list tab-pane fade" id="single-tab2">
                                <img src="{{asset('images/family_room/'.$image->images)}}" class="img-fluid img-thumbnail" alt="" />
                            </div>
                            @elseif($i==3)
                            <div class="product-tab-list tab-pane fade" id="single-tab3">
                                <img src="{{asset('images/family_room/'.$image->images)}}" alt="" class="img-fluid img-thumbnail" />
                            </div>
                            @elseif($i==4)
                            <div class="product-tab-list tab-pane fade" id="single-tab4">
                                <img src="{{asset('images/family_room/'.$image->images)}}" alt="" class="img-fluid img-thumbnail" />
                            </div>
                            @endif

                            @php
                                $i++
                            @endphp
                            @endforeach
                        </div>

                        <ul id="single-product-tab">
                            @php
                                $i=1
                            @endphp


                            @foreach($family->images as $image)


                            @if($i==1)
                            <li class="active">
                                <a href="#single-tab1"><img src="{{asset('images/family_room/'.$image->images)}}" alt="" class="cute-img" /></a>
                            </li>
                            @elseif($i==2)
                            <li>
                                <a href="#single-tab2"><img src="{{asset('images/family_room/'.$image->images)}}" alt="" class="cute-img"/></a>
                            </li>
                            @elseif($i==3)
                            <li>
                                <a href="#single-tab3"><img src="{{asset('images/family_room/'.$image->images)}}" alt="" class="cute-img"/></a>
                            </li>
                            @elseif($i==4)
                            <li>
                                <a href="#single-tab4"><img src="{{asset('images/family_room/'.$image->images)}}" alt="" class="cute-img"/></a>
                            </li>
                            @endif


                            @php
                                $i++
                            @endphp


                            @endforeach
                        </ul>

                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                        <div class="single-product-details res-pro-tb">
                            <div class="single-regular" style="text-align:center;">
                                <h1>{{ $family->title}}</h1><h1 >Form : {{ $family->month}},2019</h1><h1 >Religion : {{ $family->religion}}</h1><hr>
                            </div>
                            <div class="single-pro-price">
                                <span class="single-regular">{{ $family->room_status}} Rent : {{$family->room_rent}} Taka</span>
                            </div>
                            <div class="single-pro-cn">
                                <h6 class="single-regular">Room Quality: </h6>
                                <p style="text-align: left;font-size: 20px;"><span style="margin-right: 30px"> Total Room : {{ $family->total_room}}</span>,<span style="margin-right: 30px"> Bed Room : {{ $family->bed_room}}</span>,<span style="margin-right: 30px"> Dinning Room : {{ $family->dinning_room}}</span>,<br><span style="margin-right: 30px"> Drawing Room : {{ $family->drawing_room}}</span>,<span style="margin-right: 30px"> Bath Room : {{ $family->bath_room}}</span>,<span style="margin-right: 30px">Room  Size: {{ $family->room_size}}</span></p>

                            </div>
                            <div class="color-quality-pro">
                                <div class="clear"></div>
                                <div class="single-pro-button">
                                    <div class="pro-button">
                                        <a href="#">Booking</a>
                                    </div>
                                    <div class="pro-viwer">
                                        <a href="{{ route('families.create')}}"><i class="fa fa-heart"></i></a>
                                        <a href="{{route('families-edit', $family->id)}}""><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="single-pro-cn">
                                <span class="single-regular" style="color: green">Address Details</span>
                                <h4><span> City : {{ $family->city->name}}</span><br><span> Thana : {{ $family->thana->name}}</span><br><span> Ward : {{ $family->ward->name}}</span></h4>
                                <h4><span>{{ $family->address}}</span></h4>
                                <h4><span>{{ $family->mobile}}</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Single pro tab End-->
        <!-- Single pro tab review Start-->
        <div class="single-pro-review-area mt-t-30 mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul id="myTab" class="tab-review-design">
                            <li class="active"><a href="#Facilities">Facilities</a></li>
                            <li style="margin-left: 20px"><a href="#Conditions">Conditon</a></li>
                            <li style="margin-left: 20px"><a href="#SortDetails">Short Description</a></li>
                            <li style="margin-left: 20px"><a href="#Comment">Comment</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div class="product-tab-list product-details-ect tab-pane fade active in" id="Facilities">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <h3>{{$family->facilities}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="Conditions">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <h3>{{$family->conditions}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="SortDetails">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <h3>{{$family->description}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="product-tab-list tab-pane fade" id="Comment">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div class="review-content-section">
                                                <div class="card-block">
                                                    <div class="text-muted f-w-400">
                                                        <p>No Comment yet.</p>
                                                    </div>
                                                    <div class="m-t-10">
                                                        <div class="txt-primary f-18 f-w-600">
                                                            <p>Your Rating</p>
                                                        </div>
                                                        <div class="stars stars-example-css detail-stars">
                                                            <div class="review-rating">
                                                                <fieldset class="rating">
                                                                    <input type="radio" id="star5" name="rating" value="5">
                                                                    <label class="full" for="star5"></label>
                                                                    <input type="radio" id="star4half" name="rating" value="4 and a half">
                                                                    <label class="half" for="star4half"></label>
                                                                    <input type="radio" id="star4" name="rating" value="4">
                                                                    <label class="full" for="star4"></label>
                                                                    <input type="radio" id="star3half" name="rating" value="3 and a half">
                                                                    <label class="half" for="star3half"></label>
                                                                    <input type="radio" id="star3" name="rating" value="3">
                                                                    <label class="full" for="star3"></label>
                                                                    <input type="radio" id="star2half" name="rating" value="2 and a half">
                                                                    <label class="half" for="star2half"></label>
                                                                    <input type="radio" id="star2" name="rating" value="2">
                                                                    <label class="full" for="star2"></label>
                                                                    <input type="radio" id="star1half" name="rating" value="1 and a half">
                                                                    <label class="half" for="star1half"></label>
                                                                    <input type="radio" id="star1" name="rating" value="1">
                                                                    <label class="full" for="star1"></label>
                                                                    <input type="radio" id="starhalf" name="rating" value="half">
                                                                    <label class="half" for="starhalf"></label>
                                                                </fieldset>
                                                            </div>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                    <div class="input-group mg-b-15 mg-t-15">
                                                        <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                                        <input type="text" class="form-control" placeholder="User Name">
                                                    </div>
                                                    <div class="input-group mg-b-15">
                                                        <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                                        <input type="text" class="form-control" placeholder="Last Name">
                                                    </div>
                                                    <div class="input-group mg-b-15">
                                                        <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                                        <input type="text" class="form-control" placeholder="Email">
                                                    </div>
                                                    <div class="form-group review-pro-edt">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                                            </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
</div>
@endforeach
@endsection
