@extends('backend.admin.layouts.master')
@section('content')
<!-- <img src="{{asset('images/bechelor_room/bechelor_room.jpg')}}" height="120px" width="100%"> -->
<div class="col-sm-10 col-sm-offset-1">
    
    {{ Form::open([
    'route' => ['families-update',$families->id],
    'method' => 'POST',
    'class'=>'form-horizontal form-horizontal row-fluid',
    'enctype'=>'multipart/form-data'
    ])
    }}
    <div class="form-group" style="margin-top: 20px;">
        <label for="category_id" class="col-sm-4">{{ __('Room Category') }}</label>
        <div class="col-sm-8">
            <select name="category_id" type="category_id" id="category_id" class="form-control{{ $errors->has('category_id') ? ' is-valid' : '' }}" value="{{ old('category_id') }}">
                <option value="0" disabled="true" selected="true">===Select Category===</option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}" {{ $category->id ==$families->category_id ? 'selected' : ''}}>{{$category->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('category_id'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="title" class="col-sm-4">{{ __('Title') }}</label>
        <div class="col-sm-8">
            <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-valid' : '' }}" name="title" value="{{ $families->title}}">
            @if ($errors->has('title'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="room_status" class="col-sm-4">{{ __('Room Status') }}</label>
        <div class="col-sm-8">
            <select name="room_status" type="room_status" id="room_status" class="form-control{{ $errors->has('room_status') ? ' is-valid' : '' }}" value="{{ old('room_status') }}">
                <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
                <option value="Flat" @if ($families->room_status == "Flat")selected="selected" @endif>Flat</option>
                <option value="Semi-ripe house" @if ($families->room_status == "Semi-ripe house")selected="selected" @endif>Semi-ripe house</option>
            </select>
            @if ($errors->has('room_status'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('room_status') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="month" class="col-sm-4">{{ __('Month') }}</label>
        <div class="col-sm-8">
            <select name="month" type="month" id="month" class="form-control{{ $errors->has('month') ? ' is-valid' : '' }}" value="{{ old('month') }}">
                <option value="0" disabled="true" selected="true">===Room Available From===</option>
                <option value="January" @if ($families->month == "January")selected="selected" @endif>January</option>
                <option value="February" @if ($families->month == "February")selected="selected" @endif>February </option>
                <option value="March" @if ($families->month == "March")selected="selected" @endif> March </option>
                <option value="April" @if ($families->month == "April")selected="selected" @endif> April </option>
                <option value="May" @if ($families->month == "May")selected="selected" @endif> May </option>
                <option value="June"@if ($families->month == "June")selected="selected" @endif>June </option>
                <option value="July"@if ($families->month == "July")selected="selected" @endif>July </option>
                <option value="August"@if ($families->month == "August")selected="selected" @endif>August </option>
                <option value="September"@if ($families->month == "September")selected="selected" @endif>September </option>
                <option value="October"@if ($families->month == "October")selected="selected" @endif>October </option>
                <option value="November"@if ($families->month == "November")selected="selected" @endif>November </option>
                <option value="December"@if ($families->month == "December")selected="selected" @endif>December</option>
                <option value="No Condition"@if ($families->month == "No Condition")selected="selected" @endif>No Condition</option>
            </select>
            @if ($errors->has('month'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('month') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="religion" class="col-sm-4">{{ __('Religion') }}</label>
        <div class="col-sm-8">
            <select name="religion" type="religion" id="religion" class="form-control{{ $errors->has('religion') ? ' is-valid' : '' }}" value="{{ old('religion') }}">
                <option value="0" disabled="true" selected="true">===Family Religion===</option>
                <option value="Islam"@if ($families->religion == "Islam")selected="selected" @endif >Islam</option>
                <option value="Hinduism"@if ($families->religion == "Hinduism")selected="selected" @endif >Hinduism</option>
                <option value="Buddhism"@if ($families->religion == "Buddhism")selected="selected" @endif >Buddhism</option>
                <option value="Christianity"@if ($families->religion == "Christianity")selected="selected" @endif >Christianity</option>
                <option value="No Condition"@if ($families->religion == "No Condition")selected="selected" @endif>No Condition</option>
            </select>
            @if ($errors->has('religion'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('religion') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="total_room" class="col-sm-4">{{ __('Total Room') }}</label>
        <div class="col-sm-8">
            <select name="total_room" type="total_room" id="total_room" class="form-control{{ $errors->has('total_room') ? ' is-valid' : '' }}" value="{{ old('total_room') }}">
                <option value="0" disabled="true" selected="true">===Family Total Room===</option>
                <option value="1" @if ($families->total_room == "1")selected="selected" @endif >One</option>
                <option value="2" @if ($families->total_room == "2")selected="selected" @endif>Two</option>
                <option value="3" @if ($families->total_room == "3")selected="selected" @endif>Three</option>
                <option value="4" @if ($families->total_room == "4")selected="selected" @endif>Four</option>
                <option value="5"@if ($families->total_room == "5")selected="selected" @endif>Five</option>
                <option value="6"@if ($families->total_room == "6")selected="selected" @endif >Six</option>
                <option value="7" @if ($families->total_room == "7")selected="selected" @endif>Seven</option>
                <option value="8"@if ($families->total_room == "8")selected="selected" @endif >Eight</option>
            </select>
            @if ($errors->has('total_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('total_room') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="bed_room" class="col-sm-4">{{ __('How Many Bed Room') }}</label>
        <div class="col-sm-8">
            <select name="bed_room" type="bed_room" id="bed_room" class="form-control{{ $errors->has('bed_room') ? ' is-valid' : '' }}" value="{{ old('bed_room') }}">
                <option value="0" disabled="true" selected="true">===Select Bed Room Available===</option>
                <option value="1" @if ($families->bed_room == "1")selected="selected" @endif >One</option>
                <option value="2" @if ($families->bed_room == "2")selected="selected" @endif>Two</option>
                <option value="3" @if ($families->bed_room == "3")selected="selected" @endif>Three</option>
                <option value="4" @if ($families->bed_room == "4")selected="selected" @endif>Four</option>
                <option value="5"@if ($families->bed_room == "5")selected="selected" @endif>Five</option>
                <option value="6"@if ($families->bed_room == "6")selected="selected" @endif >Six</option>
            </select>
            @if ($errors->has('bed_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('bed_room') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="bath_room" class="col-sm-4">{{ __('How Many Bath Room') }}</label>
        <div class="col-sm-8">
            <select name="bath_room" type="bath_room" id="bath_room" class="form-control{{ $errors->has('bath_room') ? ' is-valid' : '' }}" value="{{ old('bath_room') }}">
                <option value="0" disabled="true" selected="true">===Select Available Bath Room===</option>
               <option value="1" @if ($families->bath_room == "1")selected="selected" @endif >One</option>
                <option value="2" @if ($families->bath_room == "2")selected="selected" @endif>Two</option>
                <option value="3" @if ($families->bath_room == "3")selected="selected" @endif>Three</option>
                <option value="4" @if ($families->bath_room == "4")selected="selected" @endif>Four</option>
                <option value="5"@if ($families->bath_room == "5")selected="selected" @endif>Five</option>
            </select>
            @if ($errors->has('bath_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('bath_room') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="dinning_room" class="col-sm-4">{{ __('Dinning Room') }}</label>
        <div class="col-sm-8">

            <div class="row">
                <label class="col-sm-2 col-sm-offset-2">
                    <input type="radio" name="dinning_room" value="Yes" @if($families->dinning_room=='Yes') checked @endif>Yes
                </label>
                <label class="col-sm-2 col-sm-offset-2">
                     <input type="radio" name="dinning_room" value="No" @if($families->dinning_room=='No') checked @endif>No
                </label>
            </div>


            @if ($errors->has('dinning_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('dinning_room') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label for="room_size" class="col-sm-4">{{ __('Room Size') }}</label>
        <div class="col-sm-8">
            <input id="room_size" type="number" class="form-control{{ $errors->has('room_size') ? ' is-valid' : '' }}" name="room_size" value="{{ $families->room_size }}">
            @if ($errors->has('room_size'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('room_size') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="drawing_room" class="col-sm-4">{{ __('Drawing Room') }}</label>
        <div class="col-sm-8">
            <div class="row">
                <label class="col-sm-2 col-sm-offset-2">
                    <input type="radio" name="drawing_room" value="Yes" @if($families->drawing_room=='Yes') checked @endif>Yes
                </label>
                <label class="col-sm-2 col-sm-offset-2">
                    <input type="radio" name="drawing_room" value="No" @if($families->drawing_room=='No') checked @endif>No
                </label>
            </div>
            
            @if ($errors->has('drawing_room'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('drawing_room') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="room_rent" class="col-sm-4">{{ __('Room Rent') }}</label>
        <div class="col-sm-8">
            <input id="room_rent" type="number" class="form-control{{ $errors->has('room_rent') ? ' is-valid' : '' }}" name="room_rent" value="{{ $families->room_rent }}">
            @if ($errors->has('room_rent'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('room_rent') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-sm-4">{{ __('Short Description') }}</label>
        <div class="col-sm-8">
            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-valid' : '' }}" name="description" value="{{ old('description') }}" cols="40" rows="5">{{ $families->description }}</textarea>
            @if ($errors->has('description'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="facilities" class="col-sm-4">{{ __('Room facilities') }}</label>
        <div class="col-sm-8">
            <label class="checkbox-inline" >
                <input type="checkbox" name="facilities[]" value="Fully Decorated" @if(in_array('Fully Decorated', $facilities)) checked @endif/>Fully Decorated
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="Attach Bathroom" @if(in_array('Attach Bathroom', $facilities)) checked @endif />Attach Bathroom
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="A Balcony along with the room" @if(in_array('A Balcony along with the room', $facilities)) checked @endif/>A Balcony along
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="24 Hours Water and Gass Supply" @if(in_array('24 Hours Water and Gass Supply', $facilities)) checked @endif />24 Hours Water  & guss
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="A quiet and Lovely environment" @if(in_array('A quiet and Lovely environment', $facilities)) checked @endif />A quiet and Lovely environment
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="Wifi" @if(in_array('Wifi', $facilities)) checked @endif/>Wifi
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="Security Guard" @if(in_array('Security Guard', $facilities)) checked @endif/>Security Guard</br>
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="facilities[]" value="Tiles" @if(in_array('Tiles', $facilities)) checked @endif />Tiles</br>
            </label>
            <br>
            @if ($errors->has('facilities'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('facilities') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="conditions" class="col-sm-4">{{ __('Room Conditions') }}</label>
        <div class="col-sm-8 checkbox-inline">
            <label class="checkbox-inline">
                <input type="checkbox" name="conditions[]" value="Must be Nonsmoker" @if(in_array('Must be Nonsmoker', $conditions)) checked @endif/>Must be Nonsmoker
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="conditions[]" value="Must be maintain of role of Room" @if(in_array('Must be maintain of role of Room', $conditions)) checked @endif/>Must be maintain of role of Room
            </label>
            <br>
            <label class="checkbox-inline">
                <input type="checkbox" name="conditions[]" value="Before 11PM come back" @if(in_array('Before 11PM come back', $conditions)) checked @endif/>Before 11PM come back
            </label>
            <label class="checkbox-inline">
                <input type="checkbox" name="conditions[]" value="Before 11PM come back"@if(in_array('Before 11PM come back', $conditions)) checked @endif />Before 11PM come back</br>
            </label>
            <br>
            @if ($errors->has('conditions'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('conditions') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="mobile" class="col-sm-4">{{ __('Phone Number') }}</label>
        <div class="col-sm-8">
            <input id="mobile" type="number" class="form-control{{ $errors->has('mobile') ? ' is-valid' : '' }}" name="mobile" value="{{ $families->mobile }}">
            @if ($errors->has('mobile'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('mobile') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="city_id" class="col-sm-4">{{ __(' City Name') }}</label>
        <div class="col-sm-8">
            <select name="city_id" type="text" id="city_id" class="form-control{{ $errors->has('city_id') ? ' is-valid' : '' }}" value="{{ old('city_id') }}">
                <option value="0" disabled="true" selected="true">===Select City===</option>
                @foreach($cities as $city)
                <option value="{{$city->id}}" {{ $city->id ==$families->city_id ? 'selected' : ''}}>{{$city->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('city_id'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="thana_id" class="col-sm-4">{{ __(' Thana Name') }}</label>
        <div class="col-sm-8">
            <select name="thana_id" type="text" id="thana_id" class="form-control{{ $errors->has('thana_id') ? ' is-valid' : '' }}" value="{{ old('thana_id') }}">
                <option value="0" disabled="true" selected="true">===Select Thana===</option>
            </select>
            @if ($errors->has('thana_id'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('thana_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="ward_id" class="col-sm-4">{{ __(' Ward Name') }}</label>
        <div class="col-sm-8">
            <select name="ward_id" type="text" id="ward_id" class="form-control{{ $errors->has('ward_id') ? ' is-valid' : '' }}" value="{{ old('ward_id') }}">
                <option value="0" disabled="true" selected="true">===Select Ward===</option>
            </select>
            @if ($errors->has('ward_id'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('ward_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-4">{{ __('Address') }}</label>
        <div class="col-sm-8">
            <textarea id="address" class="form-control{{ $errors->has('address') ? ' is-valid' : '' }}" name="address" value="{{ old('address') }}" cols="40" rows="5">{{ $families->address }}</textarea>
            @if ($errors->has('address'))
            <span class="btn-danger" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
            @endif
        </div>
    </div>
    
    <div class="form-group">
        <button class="btn btn-success" type="submit" style="text-align: center;">Create Post For Family</button>
    </div>
    
    {{ Form::close() }}
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
$('select[name="city_id"]').on('change',function(){
var city_id=$(this).val();
//alert(city_id);
if(city_id){
$.ajax({
url:'{{ url('')}}/cities/ajax/'+city_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="thana_id"]').empty();
$.each(data,function(key,value){
$('select[name="thana_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="thana_id"]').empty();
}
});
$('select[name="thana_id"]').on('change',function(){
var thana_id=$(this).val();
//alert(thana_id);
console.log(thana_id);
if(thana_id){
$.ajax({
url:'{{ url('')}}/thanas/ajax/'+thana_id,
type:"GET",
dataType:"json",
success:function(data){
$('select[name="ward_id"]').empty();
$.each(data,function(key,value){
$('select[name="ward_id"]').append('<option value="'+key+'">'+value+'</option>')
});
}
});
}else{
$('select[name="ward_id"]').empty();
}
});
});
</script>
@endsection