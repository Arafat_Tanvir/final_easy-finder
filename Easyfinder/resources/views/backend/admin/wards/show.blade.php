@extends('Backend.admin.layouts.master')

@section('content')
<div class="container">
  <div class="card mx-auto mt-5">
    <div class="card-header">
      <h2>Wards Create Form <span class="pull-right"><a href="{{ route('wards.index')}}">Back</a></span></h2>
    </div>
    <div class="card-body">
      <h4>Ward Details<a href="{{route('wards.index')}}" class="btn btn-primary pull-right">Back</a></h4>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-3">
                   <p>Name</p>
                   <p>Bangla Name</p>
                   <p>Latitude </p>
                   <p>Longitude</p>
            </div>
            <div class="col-sm-9">
                   <p>City: {{$wards->thana->city->name}}, Thana: {{$wards->thana->name}}, Ward: {{$wards->name}} </p>
                   <p>{{$wards->bangla_name?$wards->bangla_name:'N\A'}} </p>
                   <p>{{$wards->latitude?$wards->latitude:'N\A'}} </p>
                   <p>{{$wards->longitude?$wards->longitude:'N\A'}} </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div id="map">

  </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
