@extends('layouts.app')
@section('content')
  <script type="text/javascript" src="{{ url('')}}/assets/js/jquery-3.2.1.min.js"></script>
  <div class="content">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 offset-xs-3 offset-sm-3 offset-md-3">
          @if((Session::get('message')))
          <div class="alert alert-success alert-dismissable">
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('message')}}
            </div>
          </div>
          @endif
          <div class="card">
            <div class="card-header" style="text-align: center; background-color: #0321fd; color: #ffffff">
              <strong>Bechelor Room Create/Post Form</strong>
            </div>
            <div class="card-body card-block">
              <form role="form" action="{{ route('search_post.search_post')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field()}}
                <div class="form-group">
                  <label for="city">Select City</label>
                  <select name="city" id="city" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose City==</option>
                    @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->city}}</option>
                    @endforeach
                  </select>
                  <p class="help-block">{{ ($errors->has('city')) ? $errors->first('city') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="thana">Select Thana</label>
                  <select name="thana" id="thana" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose Thana==</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('thana')) ? $errors->first('thana') : ''}}</p>
                </div>
                <div class="form-group">
                  <label for="ward">Select Ward</label>
                  <select name="ward" id="ward" class="form-control">
                    <option value="0" disabled="true" selected="true">===Choose Ward==</option>
                  </select>
                  <p class="help-block">{{ ($errors->has('ward')) ? $errors->first('ward') : ''}}</p>
                </div>
                <button type="submit" class="btn btn-primary">Search</button>
              </form>
              <script type="text/javascript">
              $(document).ready(function(){
              $('select[name="city"]').on('change',function(){
              var cityid=$(this).val();
              if(cityid){
              $.ajax({
              url:'{{ url('')}}/cities/ajax/'+cityid,
              type:"GET",
              dataType:"json",
              success:function(data){
              $('select[name="thana"]').empty();
              $.each(data,function(key,value){
              $('select[name="thana"]').append('<option value="'+key+'">'+value+'</option>')
              });
              }
              });
              }else{
              $('select[name="thana"]').empty();
              }
              });
              $('select[name="thana"]').on('change',function(){
              var thanaid=$(this).val();
              console.log(thanaid);
              if(thanaid){
              $.ajax({
              url:'{{ url('')}}/thanas/ajax/'+thanaid,
              type:"GET",
              dataType:"json",
              success:function(data){
              $('select[name="ward"]').empty();
              $.each(data,function(key,value){
              $('select[name="ward"]').append('<option value="'+key+'">'+value+'</option>')
              });
              }
              });
              }else{
              $('select[name="ward"]').empty();
              }
              });
              });
              </script>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection