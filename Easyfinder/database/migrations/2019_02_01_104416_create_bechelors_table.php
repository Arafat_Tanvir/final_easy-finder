<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBechelorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bechelors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('month');
            $table->string('status');
            $table->string('gender');
            $table->string('religion');
            $table->string('married_status');
            $table->String('seat');
            $table->string('room_type');
            $table->float('room_rent', 8, 2);
            $table->text('facilities');
            $table->text('conditions');
            $table->string('mobile');
            $table->text('address');

            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->unsignedInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');

            $table->unsignedInteger('thana_id');
            $table->foreign('thana_id')->references('id')->on('thanas')->onDelete('cascade');

            $table->unsignedInteger('ward_id');
            $table->foreign('ward_id')->references('id')->on('wards')->onDelete('cascade');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bechelors');
    }
}
