<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubLetimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub-letimages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('images');
            $table->integer('sublet_id')->unsigned();
            $table->foreign('sublet_id')->references('id')->on('sub-lets')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub-letimages');
    }
}
