<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('month');
            $table->string('religion');
            $table->String('room_status');
            $table->string('total_room');
            $table->float('bed_room');
            $table->float('bath_room');
            $table->String('dinning_room');
            $table->String('room_rent');
            $table->String('drawing_room');
            $table->String('room_size');
            $table->text('facilities');
            $table->text('conditions');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->unsignedInteger('thana_id');
            $table->foreign('thana_id')->references('id')->on('thanas')->onDelete('cascade');
            $table->unsignedInteger('ward_id');
            $table->foreign('ward_id')->references('id')->on('wards')->onDelete('cascade');
            $table->unsignedInteger('admin_id');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
            $table->text('description');
            $table->string('mobile');
            $table->text('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
