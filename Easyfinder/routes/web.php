<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('backend.user.welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/token/{token}','Backend\VerificationController@verify')->name('user.verification');


Route::group(['prefix'=>'admin'],function(){
	//admin dashboard
	Route::get('/', 'Backend\AdminController@index')->name('admin.dashboard');

    //admin registration and verification
    Route::get('/register/submit','Auth\Admin\RegisterController@showRegistrationForm')->name('admin.register.submit');
	Route::post('/register','Auth\Admin\RegisterController@register')->name('admin.register');
	Route::get('/token/{token}', 'Backend\AdminVerificationController@verify')->name('admin.verification');
    //admin login and logout
	Route::get('/login','Auth\Admin\LoginController@showLoginForm')->name('admin.login');
	Route::post('/login/submit','Auth\Admin\LoginController@login')->name('admin.login.submit');
	Route::post('/logout','Auth\Admin\LoginController@logout')->name('admin.logout');
    //admin Forgotpassword and reset password
	Route::post('/password/email','Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('/password/reset','Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::get('/password/reset/{token}','Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
	Route::post('/password/reset','Auth\Admin\ResetPasswordController@reset')->name('admin.password.reset.post');
	Route::resource('divisions','Backend\DivisionController');
	Route::resource('districts','Backend\DistrictController');
	Route::resource('upazilas','Backend\UpazilaController');
	Route::resource('unions','Backend\UnionController');
	Route::resource('cities','Backend\CityController');
	Route::resource('thanas','Backend\ThanaController');
	Route::resource('wards','Backend\WardController');
	Route::resource('comments','Backend\CommentController');
	Route::resource('bechelors','Backend\BechelorController');




	//for categories
	Route::group(['prefix'=>'categories'],function(){
	    Route::get('/',"Backend\CategoryController@index")->name('categories.index');
	    Route::get('/create',"Backend\CategoryController@create")->name('categories.create');
	    Route::post('/store',"Backend\CategoryController@store")->name('categories.store');
	    Route::get('/show/{id}',"Backend\CategoryController@show")->name('categories.show');
	    Route::get('/edit/{id}',"Backend\CategoryController@edit")->name('categories.edit');
	    Route::post('/update/{id}',"Backend\CategoryController@update")->name('categories.update');
	    Route::post('/delete/{id}',"Backend\CategoryController@delete")->name('categories.delete');

	});


	//order route details
	Route::group(['prefix'=>'orders'],function(){

		Route::get('/',"Backend\OrderController@index")->name('orders.index');
		Route::get('/create',"Backend\OrderController@create")->name('orders.create');
		Route::post('/store',"Backend\OrderController@store")->name('orders.store');
		Route::get('/show/{id}',"Backend\OrderController@show")->name('orders.show');
		Route::get('/edit/{id}',"Backend\OrderController@edit")->name('orders.edit');
		Route::post('/update/{id}',"Backend\OrderController@update")->name('orders.update');
		Route::post('/delete/{id}',"Backend\OrderController@destroy")->name('orders.destroy');

		//paid and Complete
		Route::post('/paid/{id}',"Backend\OrderController@orderPaid")->name('orders.paid');
		Route::post('/complete/{id}',"Backend\OrderController@orderComplete")->name('orders.complete');

	});
	Route::group(['prefix'=>'families'],function(){

		Route::get('/index', [
			'uses' => 'Backend\FamilyController@index',
			'as' => 'families.index'
		]);

		Route::get('/create', [
			'uses' => 'Backend\FamilyController@create',
			'as' => 'families.create'
		]);

		Route::post('/store', [
			'uses' => 'Backend\FamilyController@store',
			'as' => 'families-store'
		]);
		Route::get('/edit/{id}',[
			'uses'=>'Backend\FamilyController@edit',
			'as'=>'families-edit'
		]);

		Route::post('/update/{id}',[
			'uses'=>'Backend\FamilyController@update',
			'as'=>'families-update'
		]);

	});


	//card
	Route::group(['prefix'=>'cards'],function(){
		
		Route::get('/',"Backend\CardController@index")->name('cards.index');
		Route::get('/create',"Backend\CardController@create")->name('cards.create');
		Route::post('/store',"Backend\CardController@store")->name('cards.store');
		Route::get('/show/{id}',"Backend\CardController@show")->name('cards.show');
		Route::get('/edit/{id}',"Backend\CardController@edit")->name('cards.edit');
		Route::post('/update/{id}',"Backend\CardController@update")->name('cards.update');
		Route::post('/delete/{id}',"Backend\CardController@destroy")->name('cards.destroy');
	});

	Route::group(['prefix'=>'checkout'],function(){
		Route::get('/checkout',"Backend\CheckoutController@index")->name('checkout.index');
		Route::get('/checkout/create',"Backend\CheckoutController@create")->name('checkout.create');
		Route::post('/checkout',"Backend\CheckoutController@store")->name('checkout.store');
		Route::get('/checkout/show',"Backend\CheckoutController@show")->name('checkout.show');
		Route::get('/checkout/edit/{id}',"Backend\CheckoutController@edit")->name('checkout.edit');
		Route::post('/checkout/update/{id}',"Backend\CheckoutController@update")->name('checkout.update');
		Route::post('/checkout/delete/{id}',"Backend\CheckoutController@destroy")->name('checkout.destroy');
	});

});

//search for any thing
Route::get('/room-search','HomeController@room_search')->name('room-search');
//search city ,thana and ward
Route::get('/search', [
	'uses' => 'Backend\LocationController@search',
	'as' => 'search'
]);
// 
Route::post('/search_post/search_post', [
	'uses' => 'Backend\LocationController@search_post',
	'as' => 'search_post.search_post'
]);


//for divisions ,districts, upazilas and unions
Route::get('/divisions/ajax/{id}', [
	'uses' => 'Backend\LocationController@divisions_ajax',
	'as' => 'divisions'
]);

Route::get('/districts/ajax/{id}', [
	'uses' => 'Backend\LocationController@districts_ajax',
	'as' => 'districts'
]);

Route::get('/upazilas/ajax/{id}', [
	'uses' => 'Backend\LocationController@upazilas_ajax',
	'as' => 'upazilas'
]);


// for cities thanas and wards
Route::get('/cities/ajax/{id}', [
	'uses' => 'Backend\BechelorController@cities_ajax',
	'as' => 'cities'
]);

Route::get('/thanas/ajax/{id}', [
	'uses' => 'Backend\BechelorController@thanas_ajax',
	'as' => 'thanas'
]);

